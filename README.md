*This project is public only for presentation purposes. You have no permission to use, modify, or share the software.*

### Technologies ###

PHP7, Nette, mySQL, JS (ES5), jQuery, Bootstrap 3, bash

### Introduction ###

This is a virtual poster wall. Users can register, log in and upload posters and info. Posters can be
filtered by category and only posters for upcoming events are shown.

![demo](readme/zeveme.gif)
![db schema](readme/zveme.cz_db.png)

### Technical details ###

The admin can at any point delete posters that are no longer relevant. The upload is chunked and
thumbnails are generated on the client (data url). JS code is structured as composable UI controller
classes and non-UI classes. There is backend and frontend internationalization. Data from PHP are passed
to JS via JSON. 

![js code split](readme/js.jpg)

### Deployment ###

see deploy.sh (Apache, mySQL)