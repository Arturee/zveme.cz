<?php

namespace App\Util;
/**
 * Created by PhpStorm.
 * User: Artur
 * Date: 03-Jul-17
 * Time: 11:01 AM
 */
use Nette\Application\UI\Form;

class Boostrap3Form {
    /**
     * @param Form $form
     * @return Form
     */
    public static function enhance(Form $form){
        $form = self::renderBootstrap3($form);
        $form = self::ajaxify($form);
        return $form;
    }








    private static function ajaxify(Form $form){
        $form->getElementPrototype()->class('ajax');
        return $form;
    }
    private static function renderBootstrap3(Form $form){
        $renderer = $form->getRenderer();
        $renderer->wrappers['controls']['container'] = NULL;
        $renderer->wrappers['pair']['container'] = 'div class=form-group';
        $renderer->wrappers['pair']['.error'] = 'has-error';
        $renderer->wrappers['control']['container'] = 'div class=col-sm-9';
        $renderer->wrappers['label']['container'] = 'div class="col-sm-3 control-label"';
        $renderer->wrappers['control']['description'] = 'span class=help-block';
        $renderer->wrappers['control']['errorcontainer'] = 'span class=help-block';
        $form->getElementPrototype()->class('form-horizontal');
        foreach ($form->getControls() as $control) {
            if ($control instanceof \Nette\Forms\Controls\Button) {
                $control->getControlPrototype()->addClass(empty($usedPrimary) ? 'btn btn-primary' : 'btn btn-default');
                $usedPrimary = TRUE;
            } elseif ($control instanceof \Nette\Forms\Controls\TextBase
                || $control instanceof \Nette\Forms\Controls\SelectBox
                || $control instanceof \Nette\Forms\Controls\MultiSelectBox) {
                $control->getControlPrototype()->addClass('form-control');
            } elseif ($control instanceof \Nette\Forms\Controls\Checkbox
                || $control instanceof \Nette\Forms\Controls\CheckboxList
                || $control instanceof \Nette\Forms\Controls\RadioList) {
                $control->getSeparatorPrototype()->setName('div')->addClass($control->getControlPrototype()->type);
            }
        }
        return $form;

        //BOOTSTRAP v3 rendering in Nette
        //source:
        //https://github.com/nette/forms/blob/a0bc775b96b30780270bdec06396ca985168f11a/examples/bootstrap3-rendering.php#L58
    }

}