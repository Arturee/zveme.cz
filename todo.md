TT: 96h






TODO
- TODO user doc (where needed)
- TODO prog doc?


TODO security breakdown







========================================================
WEIRD STUFF
- upload-id is not used (always 1) (no need for it)
- session is local (because of WEDOS)
- payment table (for future)
- upload is chunked unnecessarily





========================================================
FUTURE
- go back to classic session
- use normal upload instead of chunked! (and try to use onProgress)
- think about using SQLLite for wedos!







LATER FUTURE
- for complete security, move JS to app/ where files cannot be accessed  :)
- have HTTPS with payments!
- (to safisy wedos) use FormData to upload blob as $_FILE via ajax (after being cropped)
- move test data insert elsewhere
- BUG: if connection is torn during upload, backend probably doesnt' clean session.
- add a 2 day buffer for old posters
- category.confirmed ... list unconfirmed in admin presenter and ability to confirm or roll back to Nezarazene!
- what to test? .. only stuff that won't change!
- TODO how to crop and send image to server in an efficient way?
- hide detail/image when you click on it!. And perhaps open on hover.
- selecticize for multiple cities :)
* (PosterUploader) PERHAPS handle situation when the upload is torn off (eg. user loses connection / maybe even seesion changes id)
- analyzovat multi-akcni plakatky a multi-denni. Prioritu dat datumu smazani, nikoliv datumu konani.
    - TODO limit na datum smazani (napr. 2 mesice max)
- enable login via email
- system pro filter podle lokace (Praha/Olomouc) ALE zaroven moznost zadani presne lokace (Praha Podoli Stromkova 4)
- vstupne pridat
= lokace (asi zacit s olomouckym krajem a rozsirit ...)
- very small info displayed when an image is focused
- higher quality HD images
    + HD images zoom
- TINY BUG
    - date should NOT trail on a row
- Admin:log everyone one button (for seemless deployment)
- hide ?locale=cs ... it is NOT needed
*  login via email
*  signin via faceboog/gmail/instagram/seznam...



========================================================
## FAZE 1.2.1 - admin ##
- finish other admin functionality / adminPresenter
- remove / finish multiple upload for admin
- detail editing for admin
- browse uploaded for logged in guys
- admin presenter
    - how many per category/location
    - deletion of expired posters
- bonuses
    - flashes hide automatically
    - sidebar hides automatically
    - better styling inside sidebar
- test everything
- admin: edit any info on any poster
- custom form - multiple upload


## FAZE 1.1.3 - best GUI ##
- put less posters of front page and make it nicer
- [scout]


## FAZE 1.1.4 - req ##
- web requirements
    - intuitive gui (+maybe tutorial)
    - prog doc
    - admin doc
    - install on server (with email, with db)
    - all features to be functional (hide advanced features)

## FAZE 1.1.2 - tests ##
- all closed classes
- test upload and check corner cases (expecially of UI usage)


## FAZE 1.3 - wall ##
- WALL = increase image size + image preloading + maybe thumb merging? + wall segmentation
- wall layout (time-based/ color based/ category clustering/ + injected dates?)

## FAZE 1.4 - Js ##
- find a good way of switching to minified scripts/CDN
- us cache for Js (with file watching)

## FAZE 1.5 - deploy and make sure it is good for presentation ##

## FAZE 1.6 - location ##
- LOCATION SUPPORT

## FAZE 1.7 - speed ##
- image resizing should be done in a webworker. because it blocks with very large images


## SECURITY ##
- during registration, the password should be at least a little encrypted. OR use HTTPs

## FAZE ? - [Misc] ##
- think about server-side only translation
    https://forum.nette.org/en/23700-javascript-and-translation
- LATERs
- horizontal and vertical posters
- TODO load images in a non-browser-blocking way TODO will it help?
    (they do not block rendering, fine, but tehy still make browsr spinner busy
    and the site is somehow still very busy)
- study ES 6 and maybe Babel
- delete via snippet
- google analytics
- MAYBE use:
    - jQuery file upload
    - nette port for JFU https://github.com/blueimp/jQuery-File-Upload/blob/master/server/php/UploadHandler.php
    - date http://www.eyecon.ro/bootstrap-datepicker/










## FAZE 2.0 - hard tests ##
- test against attacks (url manipulation, fake requests, ...)
- test on all newest browsers
- set up selenium


## FAZE 2.1 ##
- make login ajaxed
- dynamic / probably ajaxed preloading of posters
- great increase in load time + a litte benchmark

- use blur and css snowfall animation
- better UI, dont throw alert on bad input field, use some nice css + maybe a flash?
- potvrzeni registrace mailem. ajaxove zjistit, zda login nebo mail neexistuje jeste pred submitem ;)
- view all my uploaded posters and edit them
- edit posters for admin?
- password recovery option
- handicap pro postery, kt. nemaji datum / TODO rozmyslet bezdatumovy plakaty => pravdepodobne nizsi zed ;)
- GodseyePresenter (tlacitko na mazani trahovejch posteru)
- good layout stratedy for endorsed (probably separate wall)
- rewrite Js() as Nette/Extension, and src declarations shouldn't really be in a PHP class pls (perhaps make a Latte makro?)
- deploy to wedos
- make emails work, make logging powerful
- enable translation on every text
- TODO test and split logs => each exception its own log!
- post editing for admin
- use hashed passwords
- snappy UI + filtering
- think through and use beneficially geolocation
- debug sidebar animation
- test and debug upload also impelemtn PUT method insted of POST
- login via ajax snippet  (homepage only)
- extract power-bootstrap/ for Nette
- banning useru pro admina
- finish upload powerfully and test what happens when session breaks in the middle
- internacionalizace
- kdyz se nekdo pokusi prihlasit jako admin (ten se musi hlasit na 2x) tak mi to hodi emaila
  a budu mit tlacitko, kt. automaticky blokuje :) napr.
- skoly - univerzity/ stredni /kurzy /letenky~reklamy obecne
- obchodaky maji vlastni plakat wall s internima akcema :)
- editing pro admina (url, times, atd.)

## TODO FAZE 2.5 ##
- actually keep a little larger thumbs and enlarge on hover.
- remove Kdyby/fake/session
- statistics presenter
- bad poster finder for admin (not enough annotation for a registered poster/duplicate or
    sex content - via google vision API?)

## TODO FAZE 3 ##
- save client state (categories) in cookies /or leverage firefox etc. defualt behavior of form-state remembering
- log admins.
- fallback for unsupported APIs?
- rewrite as SPA + ajax
- check out WEDOS HDD limitation, mysql and CRON (also composer support :))
- perhaps rewrite as SPA after the prototype is complete :)
- lze priplatit dalsich 100 za dalsi endorsement, kdyz uz vsichni dost endorsuji tak se zase vyhoupnout o
stupinek vyse :)
- what is the default photo resolution o phones?
= months in date in czech or numeric-only

## TODO FAZE 4 ##
- cross-browser + selenium testy/snapshoty
- ES6 + transpiling?










========================================================
LONG FUTURE - GOOGLE MAPS
- api key: AIzaSyBuS2P4gKrNKZk48sTmQob8Y15F3ubyU1k

# WHAT I'M MISSING #
- good frontend alerts
    - not supported functions
    - file/network exceptions
    - bad usage
    - internal errors : probably bad code!
- good separation of GUI js for different roles
- a good switch between minified and non or CDN