#!/bin/bash

#
# Creates a /deployment/ folder with only the stuff that is needed for deployment.
# (Wedos specific - vendor, bower_components, config.local.neon, .htaccess for apache)
#


dep="deployment/0.1"
tomkdir=(
    log
    temp/sessions

    www/temp
    www/wall/thumb
    www/wall/hd
)
for dir in "${tomkdir[@]}"
do
	mkdir -p --verbose $dep/$dir
done

tocopy=(
    app
    bin
    composer.json
    composer.lock

    www/wedos-problem
    www/checker
    www/css
    www/font
    www/img
    www/js

    www/.htaccess
    www/.maintenance.php
    www/bower.json
    www/index.php
    www/robots.txt

    vendor
    www/bower_components
)
# LATER remove /test and /bin from app
# LATER remove /wedos-problem

for item in "${tocopy[@]}"
do
    echo "Copying code/${item} to ${dep}/${item}"
	cp -a code/$item $dep/$item
done

echo "Copying config.local.neon version for WEDOS"
rm -r $dep/app/config/config.local.neon
mv $dep/app/config/config.wedos.neon $dep/app/config/config.local.neon