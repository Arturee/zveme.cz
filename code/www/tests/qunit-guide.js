/**
 *  deepEqual(value, expected[, message]): A recursive, strict comparison that works on all the JavaScript types. The assertion passes if value and expected are identical in terms of properties, values, and they have the same prototype;
    equal(value, expected[, message]): Verify the value provided is equal the expected parameter using a non-strict comparison (==).
    notDeepEqual(value, expected[, message]): Same as deepEqual() but tests for inequality;
    notEqual(value, expected[, message]): Same as equal() but tests for inequality;
    propEqual(value, expected[, message]): A strict comparison of the properties and values of an object. The assertion passes if all the properties and the values are identical;
    strictEqual(value, expected[, message]): Verify the value provided is equal to the expected parameter using a strict comparison (===);
    notPropEqual(value, expected[, message]): Same as propEqual() but tests for inequality;
    notStrictEqual(value, expected[, message]): Same as strictEqual() but tests for inequality;
    ok(value[, message]: An assertion that passes if the first argument is truthy;
    throws(function [, expected ] [, message ]): Test if a callback throws an exception, and optionally compare the thrown error;
 
 * https://api.qunitjs.com/assert/notOk
 *
 *
 *
 * .module ~ class
 *      beforeEach
 *      afterEach
 * .test ~ function_state_result
 *
 **/




// function Car(){
//     this.seats = 4;
// }
// Car.prototype.getSeats = function() {
//     return this.seats;
// };
//
//
//
// QUnit.module( "Car", {
//     beforeEach: function( assert ) {},
//     afterEach: function( assert ) {}
// });
// QUnit.test( "getSeats_x_works", function( assert ) {
//     assert.equal(new Car().getSeats(), 4);
// });
