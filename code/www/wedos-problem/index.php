<!DOCTYPE html>

<?php
$start = microtime(true);
if ($_SERVER['REQUEST_METHOD'] === 'POST'){

?>

<html>
    <head>
        <title>nyni server prijal dlouhy text</title>
    </head>
    <body>
        <h2>Nyni server prijal POST request</h2>
        <p>Celkova velikost tela HTTP requestu = <?php echo $_SERVER['CONTENT_LENGTH'] ?> B.</p>
        <?php
            if (isset($_POST['pocet_znaku'])){
                $znakyNaServeru = $_POST['pocet_znaku'];
            } else {
                $znakyNaServeru = 'Nelze zjistit, protoze server se tvari jako by dostal request s prazdny telem. :(';
            }
        ?>
        <p>Pocet znaku "a" poslanych na server = <?php echo $znakyNaServeru ?>.</p>
        <h2>(PHP) pole $_POST:</h2>
        <p><?php print_r($_POST); ?></p>
        <p><i>Doba behu PHP: <?php echo (microtime(true) - $start);?> ms.</i></p>
    </body>
</html>

<?php } else { ?>


<html>
    <head>
        <title>Post request problem WEDOS</title>
    </head>
    <body>
        <h2>webhosting NoLimit - s dlouhym POST requestem</h2>
        <p>
            <i>post_max_size je 32MB. max_input_vars je 10 000. Lze poslat text delky 15 000 znaku, <b>ale problem je v tom,
                ze nelze poslat text delky 1MB. Proc?</b> (Bylo mi receno, ze za to muze max_input_vars, ale jak jde videt neni to pravda.)</i>
            <br>(PHP): post_max_size = <?php echo ini_get('post_max_size')?>.
            <br>(PHP): max_input_vars = <?php echo ini_get('max_input_vars')?>.
        </p>


        <form method="post">
            <input type="number" name="pocet_znaku" value="15000"/>
            <input type="text" name="dlouhy_text" value="" readonly/>
            <input type="submit" value="Odeslat dlouhy text"/>
        </form>

        <p>
            <i>Zkuste odeslat text dlouhy 15000 znaku (15 tisic). Funguje v poradku.
            <br> Pak zkuste text dlouhy 1000000 (1 milion) znaku. (To neni ani 1MB!) Uvidite, ze server tento request
            ignoruje aniz by vyhodil chybu. (Prohlizec nevyhodi chybu - muzete zkontrolovat kdyz se podivat na
                zdrojovy kod stranky.)</i>
        </p>


        <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
        <script>
            function vytvorTextDelky(n)
            {
                var pismenko = "a";
                var dlouhy_text = "";
                for (var i =0; i<n; i++){
                    dlouhy_text = dlouhy_text + pismenko;
                }
                return dlouhy_text;
            }


            $('input[name=pocet_znaku]').change(function(){
                var pozadovanaDelkaText = $(this).val();
                var dlouhyText = vytvorTextDelky(pozadovanaDelkaText);
                $('input[name=dlouhy_text]').val(dlouhyText);
            })
                .change();

        </script>
    </body>
</html>



<?php } ?>






