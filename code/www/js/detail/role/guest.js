$(function() {
    var initUic = new InitUic();
    initUic.start();
    
    $('.popup').magnificPopup({
        type:'image',
        image: {
            verticalFit: false
        }
    });
});