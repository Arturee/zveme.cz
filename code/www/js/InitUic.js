'use strict';
/**
 * Must be called first thing in $(function(){});
 *
 * @constructor
 */
function InitUic()
{
    this._flashUic = new FlashUic($('#flashes'));
    this._config = new Config($('#js-config'));
}
InitUic.prototype.constructor = InitUic;
InitUic.prototype.start = function()
{
    $.nette.init();
    //nette.ajax.js initialization


    // if (!ZVEME_CZ.Config.conf.debugmode) {
    //     console.log = function(){};
    //     //disables all console logging
    //
    // }

    var self = this;
    window.onerror = function(messageOrEvent, source, lineno, colno, error) {
        if (!error){
            return;
        }


        if (error instanceof BugError){
            self._flashUic.exception(i18n("ui.flash.bug"));
            $.post(self._config.url.bugtracker, {
                error: error.message
            });
            setTimeout(function () {
                location.reload();
                //reloads page from cache
            }, 3000);
        } else if (error instanceof ConnectionError){
            self._flashUic.exception(i18n("ui.flash.connectionDrop"));
        } else if (error instanceof UnsupportedFunctionalityError){
            self._flashUic.exception(i18n("ui.flash.unsupported"));
        }
    };
};
InitUic.prototype.getConfig = function()
{
    return this._config;
};
InitUic.prototype.getFlashUic = function()
{
    return this._flashUic;
};


