
//LATER maybe a JSON file can be supplied from the server

i18n.translator.add({
    values: {
        "ui.flash.bug": "Nastala chyba. Omlouváme se. Za pár sekund restartujeme stránku.",
        "ui.flash.connectionDrop": "Pravděpodobně vypadlo připojení k internetu.",
        "ui.flash.unsupported": "Váš prohlážeč je zastaralý a nic na něm nefunguje. Nainstalujte si prosím update (internetového prohlížeče), aby tato stránka mohla fungovat.",

        "ui.up.uic.cannotRead": "Obrázek nějak nejde nahrát. Zkontrolujte zda soubor není chybný nebo se nejedná o neyvzklý formát a zkuste znovu.",
        "ui.up.uic.wrongFileType": "Pozor, uploadovat lze pouze obrázky. Asi jste omylem kliknuli na něco co není obrázek.",
        "ui.up.uic.fileTooLarge": [[0, null, "Pozor, nelze nahrát obrázky větší než %n MB. Zkuste obrázek nejdříve zmenšit."]]
    }
});