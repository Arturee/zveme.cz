'use strict';



$(function(){
    var initUic = new InitUic();
    initUic.start();
    var config = initUic.getConfig();
    var flashUic = initUic.getFlashUic();

    var controller = new Uic(config, flashUic);
    
    $('input[type=file]').change(function(event){
        var input = $(this)[0];
        if (!input.files || !input.files[0]){
            throw new BugError('no files in file.onchange');
        }


        controller.load(input.files[0], function(){
            controller.slide(3); //img displayed and ready for crop/rotate
        });
        controller.slide(2); //spinner: img loading from disk
    });

    $('#button-crop-complete').click(function(){
        controller.crop(function(){
            controller.slide(4); //cropped img displayed and ready to input meta info
        });
        //PERHAPS spinner: crop being processed
    });


    $('#frm-uploadForm').submit(function(e) {
        //nette.live.forms validation was triggered by a previous onsubmit handler
        var areAllInputsValid = $('#frm-uploadForm .has-error').length === 0;
        // .has-error is a bootstrap-3 class, which is given to invalid fields by nette.live.forms
        if (!areAllInputsValid){
            return;
        }

        controller.slide(5); //progress bar displayed
        $.nette.ajax(
            {}, $('#frm-uploadForm'), e
            //nette.forms.js automatically sends all form data from selected form
        ).done(function () {
            controller.upload();
        });
        //CARE this is manual triggering of nette ajaxification
            //https://forum.nette.org/cs/11005-nette-ajax-js-alt-obsluha-pro-ajax-s-jquery?p=2
        //we trigger nette.ajax manually because snippets are unsuitable (we begin an invisible upload)
        //and a page reload is not wanted.




        /**
         *
         * PERHAPS handle fail also
         *
         * ).fail(function(xhr, status, msg){
        if(xhr.readyState === 4){
            // handle, check HTTP code

        } else {
            //network problem

        }
        });
         *
         *
         */
        return false;
    });

});