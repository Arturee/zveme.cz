'use strict';
/**
 * @param dataUrl
 * @param callback
 * @constructor
 */
function ImageCropper(dataUrl, callback){
    this._WIDTH_A4 = 210;  //relative width in px
    this._HEIGHT_A4 = 297; //relative height in px

    this._thumbData = null;
    this._data = null;
    this._img = new Image();
    var self = this;
    this._img.onload = function(){
        self._data = self._getPngDataUrlHD();
        self._thumbData = self._getPngDataUrlThumb();
        if (callback){
            callback();
        }
    };
    this._img.onerror = function(){
        throw new BugError('image cropper onerror called');
        //eg. the data url is a nonsense (should never happen tho)
    };
    this._img.src = dataUrl;
    //shouldn't even need not be asynchronous in this case, but formally it is
    //(only assigns a string - no loading from HDD)
}
ImageCropper.prototype.getPngDataUrlThumb = function(){
    return this._thumbData;
};
ImageCropper.prototype.getPngDataUrlHD = function(){
    return this._data;
};









ImageCropper.prototype._getPngDataUrlThumb = function(){
    var zoom = 1;
    var WIDTH = zoom * this._WIDTH_A4;
    var HEIGHT = zoom * this._HEIGHT_A4;
    return ImageCropper._getPngDataURL(this._img, WIDTH, HEIGHT);
};
ImageCropper.prototype._getPngDataUrlHD = function(){
    var zoom = 6;
    var WIDTH = zoom * this._WIDTH_A4;
    var HEIGHT = zoom * this._HEIGHT_A4;
    return ImageCropper._getPngDataURL(this._img, WIDTH, HEIGHT);
};
ImageCropper._getPngDataURL = function(image, width, height){
    var canvas = $("<canvas/>")[0];

    if (!canvas.getContext || !canvas.toDataURL){
        throw new UnsupportedFunctionalityError('!canvas.getContext OR !canvas.toDataURL');
    }


    canvas.width = width;
    canvas.height = height;
    try {
        canvas.getContext('2d').drawImage(image, 0, 0, width, height);
    } catch(err){
        throw new BugError('Cropper cannot draw to 2dcontext');
    }
    const MIME = "image/png";
    return canvas.toDataURL(MIME);

    //CARE
    //Png is used because: Only support for image/png is required in browsers.
    //User agents may support other types.
    //https://www.w3.org/TR/2011/WD-html5-20110405/the-canvas-element.html#dom-canvas-todataurl
};