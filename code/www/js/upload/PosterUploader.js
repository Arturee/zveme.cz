'use strict';
//PERHAPS make animation much more fluent ;)

/**
 * @param urlChunk
 * @param progressTracker ProgressBarUic
 * @constructor
 */
function PosterUploader(urlChunk, progressTracker){
    this._progressTracker = progressTracker;
    this._urlChunk = urlChunk;
    this._upload = null;
    this._callback = null;
}
PosterUploader.prototype.constructor = PosterUploader;
/**
 * @param upload Upload
 * @param callback
 */
PosterUploader.prototype.upload = function(upload, callback){
    this._callback = callback;
    this._upload = upload;

    const FAKE_ID = 1;
    const INITIAL_PROGRESS_PERCENTAGE = 3;
    this._uploadNextChunk(FAKE_ID);
    this._progressTracker.progress(INITIAL_PROGRESS_PERCENTAGE);
};








PosterUploader.prototype._uploadNextChunk = function(id){
    var chunk = this._upload.nextChunk(); //PERHAPS urlencode :)
    var data = {
        id: id,
        size: chunk.length,
        hdDataUrlPart: chunk
    };
    console.log(data);
    var self = this;
    $.ajax(self._urlChunk, {
        method: 'POST',
        data: data
    }).done(function(json){
        var percentUploaded = Math.ceil(100 * json.size / self._upload.getSize());
        self._progressTracker.progress(percentUploaded);
        console.log(json);
        if (json.status === 'progress'){
            self._uploadNextChunk(id);
        } else if (json.status === 'complete'){
            if (self._callback){
                self._callback();
            }
        }
    }).fail(function(xhr, status, msg){
        throw new ConnectionError(status + '; ' + msg);
    });
    //.then().fail().always();
};