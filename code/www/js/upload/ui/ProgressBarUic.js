'use strict';
/**
 *
 * @param element
 * @constructor
 */
function ProgressBarUic(element){
    this._barWrapper = $(element).find('.progress');
    this._bar = this._barWrapper.find('.progress-bar');
    this._ok = $(element).find('.ok');
}
ProgressBarUic.prototype.progress = function(percent){
    var percentText = percent + '%';
    this._bar
        .prop('aria-valuenow', percent)
        .css('width', percentText)
        .text(percentText);

    if (percent >= 100){
        this._barWrapper.addClass('hidden');
        this._ok.removeClass('hidden');
    } else {
        this._barWrapper.removeClass('hidden');
        this._ok.addClass('hidden');
    }
};


/**
 *
 *
 *
 * <div id="progressFile">
 <div class="progress">
 <div class="progress-bar progress-bar-success" role="progressbar"
 aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
 0%
 </div>
 </div>
 <div class="ok hidden">OK</div>
 </div>

 *
 *
 */