'use strict';
//PERHAPS read as dataUrl in a chunked fashion from the file?
//reader.readAsArrayBuffer(upload.file.slice(upload.complete, upload.complete + CHUNK_SIZE));
//file is not blob, but just a pointer to it
//file.slice is just a shifted + limited pointer
//the result of reader actually does the reading, takes the time and the RAM
//PERHAPS reader.onabort();




function Uic(config, flashUic){
    this._config = config;
    this._flashUic = flashUic;

    this._originalDataUrl = null;
    this._upload = null;

    /* disable forms */
    $('.disable-submit').submit(function(){
        return false;
    });

    /* enable enhanced file upload button */
    $('#button-choose-file').click(function(){
        $('#input-file-hidden').click();
        return false;
    });


    /* date picker */
    $('input[name=fakeEndDate]').datepicker({
        dateFormat: "dd.mm.yy",
        altField: $('input[name=endDate]'),
        altFormat: "yy-mm-dd",
        minDate: 0,
        wait: 10,
        onSelect: function(){
            //LATER make this less ugly and perhaps make it work on old browsers!
            //CARE this is an UGLY workaround to make nette-live-forms work!
            var event = new KeyboardEvent("keydown", {});
            $('#frm-uploadForm-fakeEndDate')[0].dispatchEvent(event);
        }
        //only future dates including today
        //PERHAPS max date
    });
    $('#frm-uploadForm-fakeEndDate').bind('input', function(){
        //CARE this is an UGLY workaround to make nette-live-forms work!
        //https://gist.github.com/brandonaaskov/1596867
        var event = new KeyboardEvent("keydown", {});
        $(this)[0].dispatchEvent(event);
    });


    /* categories */
    $('#frm-uploadForm-categoryId').val(1); //Nezarazene
    /* enable category links */
    $('#form-upload-category-new').hide();
    $('#form-upload-button-new-category').click(function(){
        $('#form-upload-category').hide();
        $('#form-upload-category-new').show();
        return false;
    });
    $('#form-upload-button-back').click(function(){
        $('#form-upload-category-new').hide();
        $('#form-upload-category').show();
        $('#frm-uploadForm input[name=categoryNew]').val('');
        return false;
    });

}
/**
 *
 * @param number
 */
Uic.prototype.slide = function(number){
    const SLIDE_PREFIX = '.slide-';
    $(SLIDE_PREFIX + (number-1)).addClass('hidden');
    $(SLIDE_PREFIX + number).removeClass('hidden');
};
/**
 *
 * @param file
 * @param callback
 */
Uic.prototype.load = function(file, callback){
    if (!window.File || !window.FileReader || !window.FileList || !window.Blob) {
        throw new UnsupportedFunctionalityError("!window.File || !window.FileReader || !window.FileList || !window.Blob");
    }

    const MAX_UPLOAD_MB = 15;
    const MAX_UPLOAD_SIZE = MAX_UPLOAD_MB * 1024 * 1024;
    //it is literally impossible to find a picture on google over 15MB (5MB even)


    if (!file.type.startsWith('image/')){
        this._backToTheBeginning(i18n('ui.up.uic.wrongFileType'));
        return;
    }
    if (file.size >= MAX_UPLOAD_SIZE){
        this._backToTheBeginning(i18n('ui.up.uic.fileTooLarge', MAX_UPLOAD_MB));
        return;
    }

    var reader = new FileReader();
    var self = this;
    reader.onload = function(){
        self._originalDataUrl = reader.result;

        var img = new Image();
        img.onload = function(){
            var uploadPreview = $('.upload-preview');
            uploadPreview.empty();
            uploadPreview.append(img);

            if (callback){
                callback();
            }
        };
        img.onerror = function(){
            self._backToTheBeginning(i18n('ui.up.uic.cannotReadImage'));
        };
        img.src = self._originalDataUrl;
    };
    reader.onerror = function(){
        self._backToTheBeginning(i18n('ui.up.uic.cannotReadImage'));
    };
    reader.readAsDataURL(file);
    //loads the image file from HDD
};
Uic.prototype.crop = function(callback){
    if(!this._originalDataUrl){
        throw new BugError('crop triggered with no data url loaded');
    }

    var self=this;
    var cropper = new ImageCropper(this._originalDataUrl, function(){

        self._upload = new Upload(cropper.getPngDataUrlHD(), cropper.getPngDataUrlThumb());
        $('input[name=sizeGoal]').val(self._upload.getSize());
        $('input[name=thumbDataUrl]').val(self._upload.getThumb());

        if (callback){
            callback();
        }
    });
};
Uic.prototype.upload = function(callback){
    if (!this._upload) {
        throw new BugError('upload triggered with no _upload data');
    }



    var progressTracker = new ProgressBarUic($('#progress-file'));
    var uploader = new PosterUploader(this._config.url.uploadChunk, progressTracker);

    uploader.upload(this._upload, callback);
};
Uic.prototype._backToTheBeginning = function(exceptionText){
    this._flashUic.exception(exceptionText);
    $('#form-choose-file input[type=file]').val('');
    $('.slide').addClass('hidden');
    $('.slide-1').removeClass('hidden');
};















// Uic.prototype.uploadMany = function(files, i, progressTracker){
//     if(i === files.length){
//         return;
//         //end recursion
//     }
//     if (!i && !progressTracker && files.length > 0){
//         i = 0;
//         progressTracker = new ProgressBarUic(ZVEMECZ_UPLOAD.progressOverall);
//         //begin recursion
//     }
//
//     var self=this;
//     this.load(files[i], function(){
//         self.crop(function(){
//             self.upload(function(){
//                 var percentComplete = Math.ceil(((i+1)*100)/files.length);
//                 progressTracker.progress(percentComplete);
//                 self.uploadMany(files, i+1, progressTracker);
//             });
//         });
//     });
// };
