'use strict';
/**
 * 
 * @param pngDataUrlHd
 * @param thumbDataUrl
 * @constructor
 */
function Upload(pngDataUrlHd, thumbDataUrl){
    this._thumb = thumbDataUrl;
    this._hd = pngDataUrlHd;
    this._size = pngDataUrlHd.length;
    this._chunkNumber = 0;
}
Upload.prototype.nextChunk = function(){
    const CHUNK_SIZE  = 1000 * 1000;
    var result = this._hd.substr(this._chunkNumber * CHUNK_SIZE, CHUNK_SIZE);
    //index overflow in substr() doesn't matter
    this._chunkNumber++;
    return result;
};
Upload.prototype.getThumb = function(){
    return this._thumb;
};
Upload.prototype.getSize = function(){
    return this._size;
};
Upload.prototype.constructor = Upload;