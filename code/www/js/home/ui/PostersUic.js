/**
 * Created by Artur on 18-Jul-17.
 */
var PostersUic = (function(){

    function _PostersUic(){}
    _PostersUic.prototype.injectDateBoxes = function(){
        var dateStrings = $('.zvemecz-poster:not(.hidden)').map(function(idx, el){
            return $(el).data('date');
        }).get();
        dateStrings = $.unique(dateStrings);
        var self=this;
        dateStrings.forEach(function(dateString){
            var firstOfDate = $('.zvemecz-poster[data-date=\"' + dateString + '\"]')[0];
            $(firstOfDate).parent().before(self._dateBox(dateString));
        });
    };
    _PostersUic.prototype.removeDateBoxes = function(){
        $('.date-box').remove();
    };






    _PostersUic.prototype._dateBox = function(date){
        date = new Date(date);
        var day = date.getUTCDate();
        var month = date.getUTCMonth() + 1;
        //dateObj.getUTCFullYear();
        return $('<div class="date-box">' + day + '. ' + month + '.</div>');
    };















    var instance = null;
    return {
        getInstance: function () {
            if (!instance) {
                instance = new _PostersUic();
            }
            return instance;
        }
    };
})();
