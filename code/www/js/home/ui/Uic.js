function Uic()
{}
Uic.prototype.constructor = Uic;
Uic.prototype.start = function()
{
    /* lib initialization */
    $.cookie.json = true;

    /* sidebar menu */
    $("#menu-toggle").click(function(e) {
        var span = $(this).find('span');

        $("#wrapper").toggleClass("toggled");
        var isNowOpened = $("#wrapper").hasClass('toggled');
        if (isNowOpened){
            span.removeClass('glyphicon-menu-right');
            span.addClass('glyphicon-menu-left');
        } else {
            span.removeClass('glyphicon-menu-left');
            span.addClass('glyphicon-menu-right');
        }

        return false;
        //e.preventDefault();
        //prevent reload of page
    });
    if ($.cookie('sidebar.isOpen', Boolean)){
        $("#wrapper").addClass("toggled");
        $('#menu-toggle span')
            .removeClass('glyphicon-menu-right')
            .addClass('glyphicon-menu-left');
    }

    /* sidebar menu - signin */
    $('#button-login').click(function(){
        $('#control-signin').toggleClass('hidden');
    });
    var isUserLoggedIn = $('#button-logout').length > 0;
    if(isUserLoggedIn){
        $.cookie('sidebar.login.isOpen', false);
    }
    if ($.cookie('sidebar.login.isOpen', Boolean)){
        $('#control-signin').removeClass('hidden');
    }




    /* sidebar menu - categories & posters */
    var categoriesUic = CategoriesUic.getInstance();
    var postersUic = PostersUic.getInstance();
    categoriesUic.change(function(){
        postersUic.removeDateBoxes();
        postersUic.injectDateBoxes();
    });
    postersUic.injectDateBoxes();














    $(window).on('beforeunload', function() {
        var isSidebarOpen = $("#wrapper").hasClass('toggled');
        var isLoginVisible = !$('#control-signin').hasClass('hidden');

        $.cookie('sidebar.isOpen', isSidebarOpen);
        $.cookie('sidebar.login.isOpen', isLoginVisible);
    });








    function location(){
        //LATER location

        if (!navigator.geolocation){
            throw new UnsupportedFunctionalityError('!navigator.geolocation');
        }

        navigator.geolocation.getCurrentPosition(function(position) {
            console.log(position.coords);
        });
    }

};