'use strict';
/**
 *
 * Data is stored in DOM elements
 *
 * @constructor
 */
var CategoriesUic = (function(){


    function _CategoriesUic(){
        this._checkboxes = $('input[type=checkbox]:not(.checkbox-static)');
        this._callback = function(){};

        //set up change handler
        var self = this;
        this._checkboxes.change(function() {
            var isChecked = $(this).is(':checked');
            var category = $(this).val();
            self._toggleCategory(category, isChecked);
            self._callback();

            if (isChecked){
                _CategoriesUic._toggleChecked($('input[value=zvemecz-none]'), false);
            } else {
                _CategoriesUic._toggleChecked($('input[value=zvemecz-all]'), false);
            }
        });

        //load last configuration from cookies
        var uncheckedCategories = $.cookie('categories.unchecked');
        if (uncheckedCategories == null){
            uncheckedCategories = [];
        }
        uncheckedCategories.forEach(function(category){
            var elem = $('input[type=checkbox][value=' + category + ']');
            _CategoriesUic._toggleChecked(elem, false);
        });

        //initialize button "All"
        $('input[value=zvemecz-all]').click(function(){
            _CategoriesUic._toggleChecked(self._checkboxes, true);
            _CategoriesUic._toggleChecked($('input[value=zvemecz-none]'), false);
        });
        //initialize button "None"
        $('input[value=zvemecz-none]').click(function(){
            _CategoriesUic._toggleChecked(self._checkboxes, false);
            _CategoriesUic._toggleChecked($('input[value=zvemecz-all]'), false);
        });
    }
    _CategoriesUic.prototype.change = function(callback){
        this._callback = callback;
    };










    _CategoriesUic.prototype._toggleCategory = function(category, isVisible){
        var postersOfCategory = $('.zvemecz-poster[data-category=' + category + ']');
        if (isVisible){
            postersOfCategory.removeClass('hidden');
        } else {
            postersOfCategory.addClass('hidden');
        }
    };
    _CategoriesUic._toggleChecked = function(elem, isChecked){
        $(elem).prop('checked', isChecked).change();
    };








    //save cookies
    $(window).on('beforeunload', function() {
        var uncheckedCategories = $('input[type=checkbox]:not(:checked):not([value=zvemecz-all])')
            .map(function(idx, elem){ return $(elem).val(); })
            .get();
        $.cookie('categories.unchecked', uncheckedCategories);
    });





    var instance = null;
    return {
        getInstance: function () {
            if (!instance) {
                instance = new _CategoriesUic();
            }
            return instance;
        }
    };
})();
