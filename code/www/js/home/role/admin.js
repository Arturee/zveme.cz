'use strict';
$(function(){
    var initUic = new InitUic();
    initUic.start();

    var config = initUic.getConfig();

    var uic = new Uic();
    uic.start();


    function deleteChosen(jQuerySelection)
    {
        var toDelete = [];
        jQuerySelection.each(function(){
            var src = $(this).attr('src');
            var id = /[0-9]+/.exec(src)[0];
            toDelete.push(id);
        });
        $.ajax({
            type: "POST",
            url: config.url.deletePosters,
            data: {
                toDelete: toDelete
            }
        }).done(function(){
            console.log('delete done');
            location.reload();
        }).fail(function(){
            console.log('delete problem');
        });
    }




    /* select mode */
    var selectMode = false;
    $('#btn-select').click(function(){
        var text = "Vyber";
        if (selectMode){
            $('#btn-delete').hide();
            $('img.mini').removeClass('to-delete');
        } else {
            $('#btn-delete').show();
            text = "Zruš výběr";
        }
        $('#btn-select').text(text);
        selectMode = !selectMode;
    });

    /* select some */
    $('img.mini').click(function(e){
        if (!selectMode){ return; }

        $(this).toggleClass('to-delete');
        return false;
        //same as
        //e.preventDefault()
        //e.stopPropagation()
    });

    /* delete selected*/
    $('#btn-delete').hide();
    $('#btn-delete').click(function(){
        deleteChosen($('img.to-delete'));
    });
});
