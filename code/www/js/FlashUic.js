'use strict';
function FlashUic(anchor)
{
    this._anchor = $(anchor);
}
FlashUic.prototype.constructor = FlashUic;
FlashUic.prototype.exception = function(message)
{
    this._show(message, 'danger');
};
FlashUic.prototype.show = function(message, type)
{
    if (!type){
        type='info';
    }
    var flash = $('<div class="alert alert-dismissable flash alert-' + type + '"></div>');
    flash.append(message);
    flash.append($('<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'));
    this._anchor.append(flash);
};
