function UnsupportedFunctionalityError(message)
{
    this.name = "InputException";
    this.message = message;
}
UnsupportedFunctionalityError.prototype = new Error();
UnsupportedFunctionalityError.prototype.constructor = UnsupportedFunctionalityError;