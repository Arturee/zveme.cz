function ConnectionError(message)
{
    this.name = "InputException";
    this.message = message;
}
ConnectionError.prototype = new Error();
ConnectionError.prototype.constructor = ConnectionError;