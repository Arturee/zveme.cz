/**
 * @param message
 * @constructor
 */
function BugError(message)
{
    this.name = "InputException";
    this.message = message;
}
BugError.prototype = new Error();
BugError.prototype.constructor = BugError;