'use strict';
function Config(jsonContainer)
{
    var parsedJson = JSON.parse($(jsonContainer).text());
    //copy properties to self
    for (var property in parsedJson) {
        if (parsedJson.hasOwnProperty(property)) {
            this[property] = parsedJson[property];
        }
    }
    Object.freeze(this);
}
Config.prototype.constructor = Config;
