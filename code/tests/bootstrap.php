<?php
/*

    RUN:
        vendor/bin/tester tests | less
        vendor/bin/tester -c tests/php.ini tests | less

        - finds and runs all test files (*.phpt) in the subtree.

    INFO:
        - nette tester does not use php.ini on purpose - to make the test environment as clean as possible.
        - to enable DI in tests, bootstrap.php is used to manually create a DIC. It also initializes the test
        environment.
        - test methods must begin with test*
        - it is a good practice to end test file names in *Test.phpt (same as in PHPUnit, JUnit, ...)
        - tester creates output/.expected,.actual files. Very useful for debugging

 */



$CODE_DIR = __DIR__ . '/..';
require $CODE_DIR . '/vendor/autoload.php';

Tester\Environment::setup();
$configurator = new Nette\Configurator();

//$configurator->setDebugMode(FALSE);
//$configurator->enableTracy(__DIR__ . '/sandbox/log');

$configurator->setTempDirectory($CODE_DIR . '/tests/sandbox/temp');
$configurator->createRobotLoader() //this is very important
    ->addDirectory($CODE_DIR . '/app')
    ->register();
$configurator->addConfig($CODE_DIR . '/app/config/config.neon');
$configurator->addConfig($CODE_DIR . '/app/config/config.local.neon');
$configurator->addParameters([
    'wwwDir'=> __DIR__ . '/sandbox/www' //imitates wwwDir for tests
]);
return $configurator->createContainer();




//
//  Assert::same($expected, $actual) - same reference
//  Assert::equal($expected, $actual) - deep equality, order of fields doesn't matter, reference doesn't matter
//      - probably fully recursive
//
//  more here: https://tester.nette.org/cs/
//