<?php
use App\Util\Path;
use Tester\Assert;
$dic = require_once 'bootstrap.php';

/**
 * Class PathTest
 *
 * CARE these tests only work on windows!
 *
 */
class PathTest extends Tester\TestCase {
    private $wwwDir = 'C:\Users\Me';
    
    public function testConstructor_works()
    {
        new Path($this->wwwDir, 'img/thumb');
    }
    public function testConstructor_leadingSlash_works()
    {
        $path = new Path($this->wwwDir, '/img/thumb');
        Assert::equal('img/thumb', $path->toUrl());
    }
    public function testConstructor_trailingSlash_works()
    {
        $path = new Path($this->wwwDir, 'img/thumb/');
        Assert::equal('img/thumb', $path->toUrl());
    }
    public function testAppend_string_works()
    {
        $path = new Path($this->wwwDir, 'img/thumb');
        $path->append('extra/tiny');
        Assert::equal('img/thumb/extra/tiny', $path->toUrl());
    }
    public function testAppend_path_works()
    {
        $path = new Path($this->wwwDir, 'img/thumb');
        $path->append(new Path($this->wwwDir, 'extra/tiny'));
        Assert::equal('img/thumb/extra/tiny', $path->toUrl());
    }
    public function testToUrl_works()
    {
        $path = new Path($this->wwwDir, 'img/thumb');
        Assert::equal('img/thumb', $path->toUrl());
    }
    public function testToLocalRelative_works()
    {
        $path = new Path($this->wwwDir, 'img/thumb');
        $expected = 'img' . DIRECTORY_SEPARATOR . 'thumb';
        Assert::equal($expected, $path->toLocalRelative());
    }
    public function testToLocal_works()
    {
        $path = new Path($this->wwwDir, 'img/thumb');
        $expected = 'C:'
            . DIRECTORY_SEPARATOR .'Users'
            . DIRECTORY_SEPARATOR . 'Me'
            . DIRECTORY_SEPARATOR . 'img'
            . DIRECTORY_SEPARATOR . 'thumb';
        Assert::equal($expected, $path->toLocal());
    }
    public function testToSafeStream_works()
    {
        $path = new Path($this->wwwDir, 'img/thumb');
        Assert::equal('nette.safe://' . $this->wwwDir . '\img\thumb', $path->toSafeStream());
    }

}



$testCase = new PathTest();
$testCase->run();
