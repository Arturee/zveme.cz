<?php
use Tester\Assert;
use App\Model\Upload;
use Nette\Utils\DateTime;

$dic = require_once 'bootstrap.php';


class UploadTest extends Tester\TestCase {
    public function setUp() {
    }



    public function testGetThumbDataURL_works(){
        $upload = new Upload(new DateTime(), 'category', null, 'fakedataurl-Thumb',
            'Prague', 'fakeDataUrl', 'Despicable me 3', 'movie at cinema',   1000,
            null);
        Assert::equal($upload->getThumbDataUrl(), 'fakedataurl-Thumb');
    }


    public function testGetSize_works(){
        $upload = new Upload(new DateTime(), 'category', null, 'fakedataurl-Thumb',
            'Prague', 'fakeDataUrl', 'Despicable me 3', 'movie at cinema',   1000,
            null);
        Assert::equal($upload->getSize(), 0);
    }

    public function testIncreateSize_works(){
        $upload = new Upload(new DateTime(), 'category', null, 'fakedataurl-Thumb',
            'Prague', 'fakeDataUrl', 'Despicable me 3', 'movie at cinema',   1000,
            null);
        $upload->increaseSize(500);
        Assert::equal($upload->getSize(), 500);
    }


    public function testIsComplete_empty_works(){
        $upload = new Upload(new DateTime(), 'category', null, 'fakedataurl-Thumb',
            'Prague', 'fakeDataUrl', 'Despicable me 3', 'movie at cinema',   1000,
            null);
        Assert::false($upload->isComplete());
    }
    public function testIsComplete_complete_works(){
        $upload = new Upload(new DateTime(), 'category', null, 'fakedataurl-Thumb',
            'Prague', 'fakeDataUrl', 'Despicable me 3', 'movie at cinema',   1000,
            null);
        $upload->increaseSize(500);
        $upload->increaseSize(500);
        Assert::true($upload->isComplete());
    }

}

$testCase = new UploadTest();
$testCase->run();
