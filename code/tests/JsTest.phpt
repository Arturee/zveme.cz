<?php
use App\Util\Js;
use Tester\Assert;
use Nette\Utils\Html;
use App\Model\Configer;
use App\Model\PathFactory;
use Nette\Caching\IStorage;

$dic = require_once 'bootstrap.php';



class JsTest extends Tester\TestCase {
    /** @var  PathFactory */
    private $pathFactory;
    /** @var  Configer */
    private $configer;
    /** @var  IStorage */
    private $cacheStorage;
    private $basePath = 'www';

    /** @Override */
    public function setUp()
    {
        /** @var Nette\DI\Container $dic */
        global $dic;
        $this->pathFactory = $dic->getByType('App\Model\PathFactory');
        $this->configer = $dic->getByType('App\Model\Configer');
        $this->cacheStorage = $dic->getByType('Nette\Caching\IStorage');
    }




    public function testConstructor_works()
    {
        $js = $this->createJs(true);
        $expected = Html::el('div');
        Assert::equal($expected, $js->toHtml($this->basePath));
    }
    public function testAddConfig_works()
    {
        $js = $this->createJs(true);
        $js->addConfig('dog', [
            'age'=>5,
            'name'=>'Gerald'
        ]);
        $expected = Html::el('div')->addHtml(
            Html::el('script')
                ->addText('{"dog":{"age":5,"name":"Gerald"}}')
                ->addAttributes([
                    'id' => 'js-config',
                    'type' => 'application/json'
                ])
        );
        $actual = $js->toHtml($this->basePath);
        Assert::equal($expected, $actual);
    }
    public function testAddJs_works()
    {
        $js = $this->createJs(true);
        $js->addJs('function shit(){ console.log("sht");}');
        $expected = Html::el('div')->addHtml(
            Html::el('script')->addText('function shit(){ console.log("sht");}')
        );
        $actual = $js->toHtml($this->basePath);
        Assert::equal($expected, $actual);
    }
    public function testAddLibs_works()
    {
        $js = $this->createJs(true);
        $paths = $this->pathFactory->createArray([
                'js/bye.js',
                'js/day.js'
            ]);
        $js->addLibs($paths);
        $expected = Html::el('div')
            ->addHtml(Html::el('script')->setAttribute('src', $this->basePath . '/js/bye.js'))
            ->addHtml(Html::el('script')->setAttribute('src', $this->basePath . '/js/day.js'));
        $actual = $js->toHtml($this->basePath);
        Assert::equal($expected, $actual);
    }
    public function testAddFiles_works()
    {
        $js = $this->createJs(true);
        $paths = $this->pathFactory->createArray([
                'js/bye.js',
                'js/day.js'
            ]);
        $js->addFiles($paths);
        $expected = Html::el('div')
            ->addHtml(Html::el('script')->setAttribute('src', $this->basePath . '/js/bye.js'))
            ->addHtml(Html::el('script')->setAttribute('src', $this->basePath . '/js/day.js'));
        $actual = $js->toHtml($this->basePath);
        Assert::equal($expected, $actual);
    }
    public function testAddFolders_works()
    {
        $js = $this->createJs(true);
        $js->addFolders([$this->pathFactory->create('js')]); //sandbox/www/js
        $expected = Html::el('div')
            ->addHtml(Html::el('script')->setAttribute('src', $this->basePath . '/js/bye.js'))
            ->addHtml(Html::el('script')->setAttribute('src', $this->basePath . '/js/day.js'));
        $actual = $js->toHtml($this->basePath);
        Assert::equal($expected, $actual);
    }
    public function testAddFolders_filesWithDifferentExtensionsArePresent_works()
    {

    }
    public function testAddFolders_recuresive_works()
    {
        //LATER
    }
    public function testSetMinify_works()
    {
        //'var a = 5;var b = 6;'
        //LATER
    }
    public function testSetUglify_works()
    {
        //LATER
    }








    private function createJs(bool $debugMode) : Js
    {
        return new Js($this->configer->getWwwDir(), $debugMode, $this->pathFactory, $this->cacheStorage);
    }
}

$testCase = new JsTest();
$testCase->run();