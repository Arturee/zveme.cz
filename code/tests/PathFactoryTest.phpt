<?php
use Tester\Assert;
use App\Model\PathFactory;
use App\Util\Path;
$dic = require_once 'bootstrap.php';




class PathFactoryTest extends Tester\TestCase {
    private $wwwDir = 'C:\Users\Me';
    
    public function testConstructor_works()
    {
        new PathFactory($this->wwwDir);
    }
    public function testCreate_works()
    {
        $pathFactory = new PathFactory($this->wwwDir);
        $path = $pathFactory->create('img/thumb');
        $expected = new Path($this->wwwDir, 'img/thumb');
        Assert::equal($expected, $path);
    }
    public function testCreateArray_works()
    {
        $pathFactory = new PathFactory($this->wwwDir);
        $paths = $pathFactory->createArray([
            'img/thumb',
            'img/hd'
        ]);
        $expected = [
            new Path($this->wwwDir, 'img/thumb'),
            new Path($this->wwwDir, 'img/hd')
        ];
        Assert::equal($expected, $paths);
    }
    public function testPathsToAbsolutePathStringArray_works()
    {
        $paths = [
            new Path($this->wwwDir, 'img/thumb'),
            new Path($this->wwwDir, 'img/hd')
        ];
        $actual = PathFactory::pathsToAbsolutePathStringArray($paths);
        $expected = [
            self::sep('C:\Users\Me\img\thumb'),
            self::sep('C:\Users\Me\img\hd')
        ];
        Assert::equal($expected, $actual);
    }
    
    



    



    private static function sep(string $pathString) : string
    {
        return str_replace(['\\'], DIRECTORY_SEPARATOR, $pathString);
    }
}


$testCase = new PathFactoryTest();
$testCase->run();
