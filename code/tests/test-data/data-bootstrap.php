<?php
$CODE_DIR = __DIR__ . '/../..';
require $CODE_DIR . '/vendor/autoload.php';
$configurator = new Nette\Configurator();

$configurator->setTempDirectory($CODE_DIR . '/temp');
$configurator->createRobotLoader() //this is very important
    ->addDirectory($CODE_DIR . '/app')
    ->register();
$configurator->addConfig($CODE_DIR . '/app/config/config.neon');
$configurator->addConfig($CODE_DIR . '/app/config/config.local.neon');
return $configurator->createContainer();
