<?php
/** @var Nette\DI\Container $dic */
$dic = require_once 'data-bootstrap.php';
/** @var \App\Model\CategoryRepository $categoryRepository */
$categoryRepository = $dic->getByType('App\Model\CategoryRepository');


$defaultCategories = [
    //1-29
    'Nezařazené',
    'Kino',
    'Sport',
    'Klub',
    'Divadlo',
    'Night club',
    'Festival',
    'Jídlo',
    'Cirkus',
    'Pro děti',
    'Zvířata',
    'Akademická konference',
    'Přednáška',
    'Fitness',
    'Welness',
    'Shopping',
    'Sleva',
    'Ples',
    'Vesnická vývěška',
    'Budhismus',
    'Závody',
    'Soutěž',
    'Beauty',
    'Víra',
    'Politické',
    'Etické',
    'Hudba' //33
];

foreach($defaultCategories as $category){
    $categoryRepository->insertCategory($category);
}

echo "FINISHED";