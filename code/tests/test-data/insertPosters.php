<?php
use App\Model\Upload;
use Nette\Utils\DateTime;
use Nette\Utils\FileSystem;

/**
 *
 * A4: 210x297
 * img
 * img/thumb
 * 1-29
 *
 * starting index of auto-increment = 500
 *
 */


/** @var Nette\DI\Container $dic */
$dic = require_once 'data-bootstrap.php';
/** @var \App\Model\PosterRepository $posterManager */
$posterManager = $dic->getByType('App\Model\PosterRepository');

function tryInsert($id, $newId, $upload)
{
    global $posterManager;
    $PATH_WALL = '../../www/wall';
    try {
        $posterManager->manualInsertPoster($newId, $upload);
        $pathHd = 'img/' . $id . '.png';
        $pathHdNew = $PATH_WALL . '/hd/' . $newId . '.png';
        $pathThumb = 'img/thumb/' . $id . '.png';
        $pathThumbNew = $PATH_WALL . '/thumb/' . $newId . '.png';
        FileSystem::copy($pathHd, $pathHdNew);
        FileSystem::copy($pathThumb, $pathThumbNew);
    } catch(Exception $e){
        echo "Could not create poster " . $id . " ...";
        echo $e->getMessage() . "\n";
    }
}




//LATER make descriptions 1-9 fitting!
$today = new DateTime();
// KINO
tryInsert(1, 1, new Upload(
    $today->modifyClone('+1 month'),
    2,
    null,
    null,
    '42.00;14.000',
    'http://www.hobbit.com',
    'Hobbit 2',
    'Bilbo se vydava na cestu za dobrodruzstvim',
    null,
    null //LATER some owner IDS
));
tryInsert(2, 2, new Upload(
    $today->modifyClone('+3 days'),
    2,
    null,
    null,
    '42.00;14.000',
    'http://www.hudba.cz',
    '38th international jazz festival praha',
    null,
    null,
    null
));
tryInsert(3, 3, new Upload(
    $today->modifyClone('+4 days'),
    2,
    null,
    null,
    '42.00;14.000',
    'http://www.kulturistika.cz',
    'Svět kulturistiky; Phil Heath',
    null,
    null,
    null
));
tryInsert(4, 4, new Upload(
    $today->modifyClone('+4 days'),
    2,
    null,
    null,
    '42.00;14.000',
    null,
    'Brainstorming',
    null,
    null,
    null
));
tryInsert(5, 5, new Upload(
    $today->modifyClone('+1 month, 7 days'),
    2,
    null,
    null,
    '42.00;14.000',
    null,
    'Tvůrčí ohlédnutí za fotografickými výstavami',
    'Lenka Županová se ohlíží za fotografickými výstavami.',
    null,
    null
));
tryInsert(6, 6, new Upload(
    $today->modifyClone('+4 days'),
    2,
    null,
    null,
    '42.00;14.000',
    'http://www.alternativa-festival.cz',
    'Alternativa',
    'festival hutné hudby',
    null,
    null
));


tryInsert(7, 7, new Upload(
    $today->modifyClone('-1 days'),
    2,
    null,
    null,
    null,
    null,
    'Bassta fideli',
    null,
    null,
    null
));
tryInsert(8, 8, new Upload(
    $today->modifyClone('+4 days'),
    2,
    null,
    null,
    null,
    null,
    'Bassta fideli',
    null,
    null,
    null
));
tryInsert(9, 9, new Upload(
    $today->modifyClone('+4 days'),
    2,
    null,
    null,
    null,
    null,
    'Bassta fideli',
    null,
    null,
    null
));





// ATLETIKA
tryInsert(10, 10, new Upload(
    $today->modifyClone('+14 days'),
    4,
    null,
    null,
    'Ostrava',
    'http://www.ostrava.com',
    'Czech indoor gala',
    'Bezecke zavody',
    null,
    null
));
tryInsert(11, 11, new Upload(
    $today->modifyClone('+18 days'),
    4,
    null,
    null,
    'Ostrava',
    null,
    'Atletika pro deti',
    'Behej skákej, házej rád.',
    null,
    null
));
tryInsert(12, 12, new Upload(
    $today->modifyClone('+19 days'),
    4,
    null,
    null,
    null,
    null,
    'Atleticka extralige 13',
    'Sponzoruje ceske sporitelna.',
    null,
    null
));
tryInsert(13, 13, new Upload(
    $today->modifyClone('+18 days'),
    4,
    null,
    null,
    'Tábor',
    'mcr2013-tabor.cz',
    'Mistrovství ČR mužů a žen v atletice',
    null,
    null,
    null
));
tryInsert(14, 14, new Upload(
    $today->modifyClone('+18 days'), //LATER trva dva dny!
    4,
    null,
    null,
    'Třinec',
    null,
    'Mistrovství ČR v atletice',
    'juniorské a dorostenecké kategorie',
    null,
    null
));
tryInsert(15, 15, new Upload(
    $today->modifyClone('+18 days'),
    4,
    null,
    null,
    'Ostrava',
    null,
    'Mistrovství České republiky juniorů, juniorek, dorostencú a dorostenek na dráze pro rok 2015',
    'městský stadion Ostrava Vítkovice',
    null,
    null
));
tryInsert(16, 16, new Upload(
    $today->modifyClone('+18 days'),
    4,
    null,
    null,
    'Praha',
    null,
    'Mistrovství ČR v halové atletice',
    'juniorů, juniorek, dorostencú a dorostenek. Lokace: Praha stromkova',
    null,
    null
));
tryInsert(17, 17, new Upload(
    $today->modifyClone('+3 days'),
    4,
    null,
    null,
    'Hodonín',
    null,
    'Mistrovství ČR atletice mužů a žen do 22 let',
    'Stadion Hodonín u červených domků',
    null,
    null
));
tryInsert(18, 18, new Upload(
    $today->modifyClone('+3 days'),
    4,
    null,
    null,
    'Praha',
    'atletika.cz',
    'Mistrovství ČR v halové atletice',
    'juniorů, juniorek, dorostencú a dorostenek. Lokace: Praha stromkova',
    null,
    null
));
tryInsert(19, 19, new Upload(
    $today->modifyClone('+3 days'), //LATER toto je MULTI-akcni plakat
    4,
    null,
    null,
    'České Budějovice',
    'atletika.cz',
    'Atletika sokol Budějovice',
    '',
    null,
    null
));



//PLES
tryInsert(20, 20, new Upload(
    $today->modifyClone('+20 days'),
    23,
    null,
    null,
    null,
    null,
    'Soutěž o maturitní plakát',
    '',
    null,
    null
));
tryInsert(21, 21, new Upload(
    $today->modifyClone('+41 days'), //LATER vstupne 130kc
    19,
    null,
    null,
    'Moravský Beroun',
    null,
    'Repre městský ples',
    'lokace: sál národního domu',
    null,
    null
));
tryInsert(22, 22, new Upload(
    $today->modifyClone('+41 days'),
    19,
    null,
    null,
    'Kopřovice',
    null,
    'Obecní ples',
    'město Kopřovice',
    null,
    null
));
tryInsert(23, 23, new Upload(
    $today->modifyClone('+12 days'),
    19,
    null,
    null,
    'Mikulov',
    null,
    '7. reprezentační ples',
    null,
    null,
    null
));
tryInsert(24, 24, new Upload(
    $today->modifyClone('+12 days'),
    19,
    null,
    null,
    'Hořice',
    null,
    'Maturitní ples',
    'vstupné 150kč',
    null,
    null
));
tryInsert(25, 25, new Upload(
    $today->modifyClone('+12 days'),
    19,
    null,
    null,
    'Zlín',
    'spszl.cz',
    '14. reprezentační ples',
    'vstupné 150kč',
    null,
    null
));
tryInsert(26, 26, new Upload(
    $today->modifyClone('+12 days'),
    19,
    null,
    null,
    'Olomouc',
    null,
    'Ples',
    'BALANC o.s.',
    null,
    null
));
tryInsert(27, 27, new Upload(
    $today->modifyClone('+12 days'),
    19,
    null,
    null,
    'Praha',
    null,
    '128. repre ples mediků',
    '1. lékařská fakulta univerzity Karlovy v Praze',
    null,
    null
));
tryInsert(28, 28, new Upload(
    $today->modifyClone('+12 days'),
    19,
    null,
    null,
    'Holešov',
    null,
    '2. ples základní školy',
     'bohatá tombola',
    null,
    null
));
tryInsert(29, 29, new Upload(
    $today->modifyClone('+12 days'),
    19,
    null,
    null,
    'Praha',
    null,
    'Společenský ples',
    'lokace: Praha-Kolovraty. vstupné 120kč',
    null,
    null
));














//
//for ($i=10; $i<=100; $i++){
//    tryInsert(10, $i, new Upload(
//        $today->modifyClone('+' . $i . ' days'),
//        'Galerie',
//        null,
//        null,
//        null,
//        null,
//        'Bassta fideli',
//        null,
//        null,
//        null
//    ));
//}

echo "FINISHED";