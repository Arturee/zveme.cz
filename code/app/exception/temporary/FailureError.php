<?php
/**
 * Created by PhpStorm.
 * User: Artur
 * Date: 27-Jun-17
 * Time: 9:00 AM
 */

namespace App\Exception;
use Exception;


/**
 * Class FailureError
 *
 * Thrown when a subsystem dependency fails (eg. cannot connect to db, cannot write to file)
 *
 * Alert admin, tell user to wait 15 min.
 *
 * @package App
 */
class FailureError extends Exception {
    public function __construct($message, $code = 0, Exception $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}