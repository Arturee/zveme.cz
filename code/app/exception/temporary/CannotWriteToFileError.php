<?php
/**
 * Created by PhpStorm.
 * User: Artur
 * Date: 13-Sep-17
 * Time: 11:27 AM
 */

namespace App\Exception;
use Exception;

class CannotWriteToFileError extends FailureError {
    public function __construct($message, $code = 0, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}