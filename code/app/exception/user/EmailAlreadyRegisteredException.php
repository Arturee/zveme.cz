<?php

/**
 * Created by PhpStorm.
 * User: Artur
 * Date: 05-Jul-17
 * Time: 5:42 PM
 *
 *
 */
namespace App\Exception;
use Exception;

class EmailAlreadyRegisteredException extends UserException {
    public function __construct($message, $code = 0, Exception $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}