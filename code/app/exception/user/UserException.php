<?php
/**
 * Created by PhpStorm.
 * User: Artur
 * Date: 27-Jun-17
 * Time: 9:05 AM
 */

namespace App\Exception;
use Exception;

/**
 * Class HackException
 *
 * Thrown when user does some nonsense move - eg. registers himself twice, etc. It doesn't event have to be the user's
 * fault really.
 *
 * Let the user retry and inform them via error/flash.
 *
 * @package App
 */
class UserException extends Exception {
    public function __construct($message, $code = 0, Exception $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}