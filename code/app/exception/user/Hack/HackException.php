<?php
/**
 * Created by PhpStorm.
 * User: Artur
 * Date: 27-Jun-17
 * Time: 9:05 AM
 */
namespace App\Exception;
use Exception;


/**
 * Class HackException
 *
 * Thrown when a user/hacker is trying to hack the site. (eg. by url-hacking or using a wrong http method)
 *
 * Log and alert admin. Do not show any info to user.
 *
 * @package App
 */
class HackException extends Exception {
    public function __construct($message, $code = 0, Exception $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}