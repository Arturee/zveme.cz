<?php
/**
 * Created by PhpStorm.
 * User: Artur
 * Date: 22-Aug-17
 * Time: 12:12 PM
 */

namespace App\Exception;
use Exception;


/**
 * Class ForbiddenAccessException
 *
 * Thrown if user is not in sufficiently high role or he uses an incorrect method (eg. HTTP method, or not ajax etc.)
 *
 * @package App\Exception
 */
class ForbiddenAccessException extends HackException {
    public function __construct($message, $code = 0, Exception $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}