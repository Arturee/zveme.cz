<?php
/**
 * Created by PhpStorm.
 * User: Artur
 * Date: 03-Jul-17
 * Time: 1:14 PM
 */
namespace App\Exception;
use Exception;

/**
 * Class NotImplementedError
 *
 * Thrown by a function/object that is not yet implemented
 *
 * @package App
 */
class NotImplementedError extends BugError {
    public function __construct($message = '', $code = 0, Exception $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}