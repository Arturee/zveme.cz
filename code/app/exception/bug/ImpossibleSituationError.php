<?php
/**
 * Created by PhpStorm.
 * User: Artur
 * Date: 21-Aug-17
 * Time: 7:55 PM
 */

namespace App\Exception;
use Exception;

/***
 * Class ImpossibleSituationError
 *
 * Thrown in impossible situations, much like Java assert. (eg. impossible branch of switch statement)
 *
 * @package App\Exception
 */
class ImpossibleSituationError extends BugError {
    public function __construct($message = '', $code = 0, Exception $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}