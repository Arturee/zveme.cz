<?php
/**
 * Created by PhpStorm.
 * User: Artur
 * Date: 03-Jul-17
 * Time: 12:53 PM
 */
namespace App\Exception;

/**
 * Class BadUsageError
 *
 * Thrown when a programmer uses code in a way it is not supposed to be used.
 *
 * @package App
 */
class BadUsageError extends BugError {
    public function __construct($message, $code = 0, \Exception $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}