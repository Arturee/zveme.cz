<?php
/**
 * Created by PhpStorm.
 * User: Artur
 * Date: 13-Sep-17
 * Time: 7:25 AM
 */

namespace App\Control;



use Kdyby\Translation\Translator;
use Nette\Application\UI\Form;

class EnhancedForm extends Form
{
    public function __construct(Translator $translator){
        parent::__construct();
        $this->setTranslator($translator);
        $this->addProtection('control.form.protection');
    }

}