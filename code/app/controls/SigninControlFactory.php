<?php
/**
 * Created by PhpStorm.
 * User: Artur
 * Date: 13-Sep-17
 * Time: 9:24 PM
 */

namespace App\Control;
use Nette;
use Nette\Security\User;
use Kdyby\Translation\Translator;
use App\Util\Logger;


class SigninControlFactory
{
    use Nette\SmartObject;

    /** @var  User */
    private $user;
    /** @var Translator */
    private $translator;
    /** @var  Logger */
    private $logger;

    /**
     * SigninControlFactory constructor.
     * @param User $user
     * @param Translator $translator
     * @param Logger $logger
     */
    public function __construct(User $user, Translator $translator, Logger $logger)
    {
        $this->user = $user;
        $this->translator = $translator;
        $this->logger = $logger;
    }

    public function create() : SigninControl
    {
        return new SigninControl($this->user, $this->translator, $this->logger);
    }

}