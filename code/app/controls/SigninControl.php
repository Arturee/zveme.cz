<?php
namespace App\Control;
use Kdyby\Translation\Translator;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Security\AuthenticationException;
use App\Util\Logger;
use Nette\Security\User;



/**
 * Class SigninControl
 * @package App\Control
 */

class SigninControl extends Control {
    /** @var callable[] */
    public $onLogin = [];
    /** @var callable[] */
    public $onLogout = [];
    /** @var User */
    private $user;
    /** @var  Translator */
    private $translator;
    /**
     * @var Logger
     */
    private $logger;

    public function __construct(User $user, Translator $translator, Logger $logger)
    {
        parent::__construct();
        $this->user = $user;
        $this->translator = $translator;
        $this->logger = $logger;
    }
    public function render()
    {
        $this->template->user = $this->user;
        $this->template->render(__DIR__ . '/SigninControl.latte');
    }
    /**
     * CARE it's ajaxified
     * @return Form
     */
    protected function createComponentSigninForm() : Form
    {
        $form = new EnhancedForm($this->translator);
        $form->addText('username', '')
            ->setRequired('ui.control.signin.required.username');
        $form->addPassword('password', '')
            ->setRequired('ui.control.signin.required.password');
        $form->addSubmit('login', 'ui.common.login');
        $form->onSuccess[] = [$this, 'signIn'];
        return $form;
    }
    /**
     * @param $form Form
     */
    public function signIn(Form $form, $values) : void
    {
        if($this->user->isLoggedIn()){
            $this->logger->logBug('Logged-in user tries to log in again. Should not be possible form GUI.'
                .'this is either a bug or a stupid hack attempt');
        } else {
            try{
                $this->user->login($values->username, $values->password);
                $this->onLogin($this);
            } catch(AuthenticationException $e){
                $form->addError($this->translator->trans('ui.control.signin.badLogin'));
            }
        }
    }
    public function handleOut()
    {
        $this->user->logout();
        $this->onLogout($this);
    }
}