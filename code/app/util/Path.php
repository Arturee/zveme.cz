<?php

namespace App\Util;
use Nette\Utils\Strings;
/**
 * Class Path
 * in latte use {$basePath}/{$path->toUrl()}
 */
class Path {
    /**
     * @var string without beginning and trailing /
     */
    private $path;
    /** @var string */
    private $wwwDir;

    /**
     * Path constructor.
     * @param string $wwwDir
     * @param string $path
     */
    public function __construct(string $wwwDir, string $path)
    {
        $this->path = self::normalizeRelative($path);
        $this->wwwDir = self::normalizeAbsolute($wwwDir);
    }
    /**
     * @param string|Path $path
     * @return $this
     */
    public function append($path) : Path
    {
        if (is_string($path)){
            $path = new Path($this->wwwDir, $path);
        }
        /** @var $path Path */
        $this->path = $this->path . '/' . $path->toUrl();
        return $this;
    }
    /**
     * Relative path
     */
    public function toUrl() : string
    {
        return $this->path;
    }
    /**
     * @return string
     */
    public function toLocalRelative() : string
    {
        return self::useLocalDirectorySeparators($this->path);
    }
    /**
     * Absolute path
     *
     * @return string
     */
    public function toLocal() : string
    {
        $result = $this->wwwDir . '/' . $this->path;
        return self::useLocalDirectorySeparators($result);
    }
    /**
     * Uses Nette Safe stream
     * which must be either loaded via composer or registered via some static function.
     */
    public function toSafeStream() : string
    {
        return 'nette.safe://' . $this->toLocal();
    }
















    private static function normalizeAbsolute(string $pathString) : string
    {
        $result = Strings::trim($pathString);
        $result = str_replace('\\', '/', $result);
        if (Strings::endsWith($result, '/')){
            $result = Strings::substring($result, 0, strlen($result)-1);
        }
        return $result;
    }
    private static function normalizeRelative(string $pathString) : string
    {
        $result = self::normalizeAbsolute($pathString);
        if (Strings::startsWith($result, '/')){
            $result = Strings::substring($result, 1);
        }
        return $result;
    }
    private static function useLocalDirectorySeparators(string $path) : string
    {
        return str_replace(array('/', '\\'), DIRECTORY_SEPARATOR, $path);
    }
}