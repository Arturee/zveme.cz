<?php
/**
 * Created by PhpStorm.
 * User: Artur
 * Date: 31-Jul-17
 * Time: 9:29 AM
 */
namespace App\Util;


class DateTime extends \Nette\Utils\DateTime
{
    /**
     * Mysql-friendly format
     * @return string
     */
    public function dbFormat(){
        return $this->format('Y-m-d H:i:s');
    }
    public static function now(){
        return new DateTime();
    }
}