<?php

/**
 *
 * Created by PhpStorm.
 * User: Artur
 * Date: 26-Jun-17
 * Time: 11:22 AM
 */
namespace App\Util;
use App\Exception\BadUsageError;
use App\Exception\NotImplementedError;
use Nette\Caching\Cache;
use Nette\Caching\IStorage;
use Nette\Utils\Html;
use Nette\Utils\Finder;
use Nette\Utils\FileSystem;
use Nette\Utils\Strings;
use App\Model\PathFactory;
use JShrink\Minifier;


/**
 * Class Js
 * @package App\Util
 *
 * LATER test comment minification
 * LATER uglifier
 * LATER recursiveness
 *
 */
class Js {
    const
        MODE_TAGS = 0,
        MODE_MINIFY = 1,
        MODE_UGLIFY = 2;

    /** @var  PathFactory */
    private $pathFactory;
    /** @var string */
    private $debugMode, $wwwDir;

    /** @var array[] */
    private $config = [];
    /** @var string */
    private $rawJs = '';
    /** @var Path[] */
    private $files = [];
    /** @var Path[] */
    private $filesLibs = [];
    /** @var string */
    private $mode = self::MODE_TAGS;
    /**
     * @var Cache
     */
    private $cache;


    public function __construct(string $wwwDir, bool $debugMode, PathFactory $pathFactory, IStorage $cacheStorage)
    {
        $this->pathFactory = $pathFactory;
        $this->debugMode = $debugMode;
        $this->wwwDir = $wwwDir;
        $this->cache = new Cache($cacheStorage); //preferred way of injecting cache according to Nette
    }
    /**
     * @param string[] $config
     */
    public function addConfig(string $namespace, array $config)
    {
        $original = [];
        if (isset($this->config[$namespace])){
            $original = $this->config[$namespace];
        }
        $this->config[$namespace] = array_merge($original, $config);
    }
    public function addJs(string $js){
        $this->rawJs .= $js;
    }
    /**
     * @param Path[] $paths
     */
    public function addLibs(array $paths)
    {
        $this->filesLibs = $this->addPaths($this->filesLibs, $paths);
    }
    /**
     * @param Path[] $paths
     */
    public function addFiles(array $paths)
    {
        $this->files = $this->addPaths($this->files, $paths);
    }
    /**
     * @param Path[] $paths
     * @param bool $recursive
     * @throws BadUsageError
     * @throws NotImplementedError
     */
    public function addFolders(array $paths, bool $recursive=false)
    {
        if ($recursive){
            throw new NotImplementedError();
        }

        $this->assertFoldersExist($paths);
        $paths = PathFactory::pathsToAbsolutePathStringArray($paths);
        $prefixSize = Strings::length($this->wwwDir);
        foreach(Finder::findFiles('*.js')->in($paths) as $file){
            $file = Strings::substring($file, $prefixSize);
            $this->files[] = $this->pathFactory->create($file);
        }
    }
    public function setMinify()
    {
        if (!$this->debugMode){
            $this->mode = self::MODE_MINIFY;
        }
    }
    public function setUglify()
    {
        if (!$this->debugMode){
            $this->mode = self::MODE_UGLIFY;
        }
    }
    public function toHtml(string $basePath) : Html
    {
        //LATER how to create a new folder in cache? - use a folder
        $html = $this->cache->load($this->getHash(), function(& $dependencies) use ($basePath) {
            //LATER set expiration via dependencies (files)
            return $this->createHtml($basePath);
        });
        return $html;
    }










    private function createHtml(string $basePath) : Html
    {
        $result = Html::el('div');
        $tagsLibs = self::createScriptTags($this->filesLibs, $basePath);
        foreach($tagsLibs as $tag){
            $result->addHtml($tag);
        }
        $tagConfig = $this->getConfigTag();
        if ($tagConfig){
            $result->addHtml($tagConfig);
        }
        if ($this->mode === self::MODE_TAGS){
            $freeJs = $this->rawJs;
            if ($freeJs){
                $tagFreeJS = Html::el('script')->addText($this->rawJs);
                $result->addHtml($tagFreeJS);
            }
            $tagsFiles = self::createScriptTags($this->files, $basePath);
            foreach($tagsFiles as $tag){
                $result->addHtml($tag);
            }
        } else {
            $js = $this->rawJs . $this->concatFiles();
            $js = self::removeUseStrictDirectives($js);
            if (Strings::length($js)!==0){
                if ($this->mode === self::MODE_MINIFY){
                    $js = self::minify($js);
                } else { //self::MODE_UGLIFY
                    $js = self::uglyfy($js);
                }
                $tag = "<script>";
                $tag .= $js;
                $tag .= "</script>";
                //CARE Html::el is not used, because addText() escapes JS, which is a problem! (eg. < escaped to &lt;)
                $result->addHtml($tag);
            }
        }
        return $result;
    }
    private function getHash() : string
    {
        $dataString = implode("+", array_map(function($el){
            return implode("=",$el);
        }, $this->config));
        $dataString .= $this->mode;
        foreach ($this->files as $file) {
            $dataString .= $file->toLocalRelative();
        }
        foreach ($this->filesLibs as $file) {
            $dataString .= $file->toLocalRelative();
        }
        $dataString .= $this->rawJs;
        return md5($dataString);
    }
    private function getConfigTag() : ?Html
    {
        if (empty($this->config)){
            return null;
        }
        $result = Html::el('script')
            ->addAttributes([
                'id'=>'js-config',
                'type'=>'application/json'
            ])
            ->addText(json_encode($this->config));
        return $result;
    }
    private function concatFiles() : string
    {
        $js = '';
        foreach($this->files as $path){
            $js .= FileSystem::read($path->toLocal());
        }
        return $js;
    }
    /**
     * @param $paths Path[]
     * @param $basePath
     * @return Html[]
     */
    private static function createScriptTags(array $paths, string $basePath) : array
    {
        $tags = [];
        foreach($paths as $path){
            $wwwPath = $basePath . '/' . $path->toUrl();
            $tags[] = Html::el('script')
                ->addAttributes(['src'=>$wwwPath]);
        }
        return $tags;
    }
    private static function uglyfy(string $js) : string
    {
        throw new NotImplementedError;
    }
    private static function minify(string $js) : string
    {
        $js = Minifier::minify($js);
        return $js;
    }
    private static function removeUseStrictDirectives(string $js) : string
    {
        $js = Strings::replace($js, "/'use strict';/", '');
        $js = Strings::replace($js, '/"use strict";/', '');
        $js = Strings::trim($js);
        return $js;
    }

    /**
     * @param Path[] $where
     * @param Path[] $paths
     * @return Path[] mixed
     * @throws BadUsageError
     */
    private function addPaths(array $where, array $paths) : array
    {
        $this->assertFilesExist($paths);
        $where = array_merge($where, $paths);
        return $where;
    }
    /**
     * @param Path[] $paths
     * @throws BadUsageError
     */
    private function assertFilesExist(array $paths) : void
    {
        /** @var Path $path */
        foreach ($paths as $path){
            $fullPath = $path->toLocal();
            if (!file_exists($fullPath)){
                throw new BadUsageError('Trying to include a non-existant script "'
                    . $path->toLocal() . '"');
            }
        }
    }
    /**
     * @param $paths Path[]
     * @throws BadUsageError
     */
    private function assertFoldersExist(array $paths) : void
    {
        foreach ($paths as $path){
            $fullPath = $path->toLocal();
            if (!is_dir($fullPath)){
                throw new BadUsageError('Trying to include a non-existant'
                 . 'folder of scripts "' . $path->toLocal() . '"');
            }
        }
    }
}
