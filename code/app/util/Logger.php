<?php

/**
 * Created by PhpStorm.
 * User: Artur
 * Date: 27-Jun-17
 * Time: 12:40 PM
 */
namespace App\Util;
use App\Exception\CannotWriteToFileError;
use App\Model\PathFactory;
use App\Model\UserRepository;
use Nette\Security\Identity;
use Nette\Security\IIdentity;
use Nette\Security\User;
use Tracy\Debugger;
use Tracy\ILogger;
use Nette\SmartObject;


/**
 * Class Logger
 * LATER maybe look into some loggin lib.
 *
 * @package App\Util
 */
class Logger {
    use SmartObject;

    /** @var PathFactory */
    private $pathFactory;
    /** @var  User */
    private $netteUser;
    /** @var  UserRepository */
    private $userRepository;

    public function __construct(PathFactory $pathFactory, User $netteUser, UserRepository $userRepository)
    {
        $this->pathFactory = $pathFactory;
        $this->netteUser = $netteUser;
        $this->userRepository = $userRepository;
    }


    public function logBug(string $message, $data = null) : void
    {
        self::logViaTracy('BUG', $message, $data, ILogger::CRITICAL);
    }
    public function logHack(string $message, $data = null) : void
    {
        self::logViaTracy('HACK', $message, $data, ILogger::CRITICAL);
    }
    public function logFailure(string $message, $data = null) : void
    {
        self::logViaTracy('FAILURE', $message, $data, ILogger::CRITICAL);
    }


    /**
     * non-repudiation log
     *
     */
    public function logAdminAction($message) : void
    {
        /** @var \App\Model\User $user */
        $user = $this->userRepository->get($this->netteUser->getId());
        $logMessage = DateTime::now()->dbFormat() . '; ';
        $logMessage .= $user->getEmail() . '; ';
        $logMessage .= $message . "\n";
        $path = $this->pathFactory->create('../log/admin-action.log');
        file_put_contents($path->toSafeStream(), $logMessage, FILE_APPEND);
    }
    /**
     * log of js errors of clients. To help bring attention to missing JS tests.
     *
     */
    public function logJsBug($message) : void
    {
        /** @var \App\Model\User $user */
        $logMessage = DateTime::now()->dbFormat() . '; ';
        $logMessage .= $message . "\n";
        $path = $this->pathFactory->create('../log/js-bug.log');
        file_put_contents($path->toSafeStream(), $logMessage, FILE_APPEND);
        //file gets automatically created

        //LATER perhaps add emails
    }








    /**
     * Is useful due to it's automatic emailing
     */
    private static function logViaTracy(string $type, string $message, $data, string $priority) : void
    {
        $message = '[' . $type . '] ' . $message;
        if ($data){
            foreach ($data as $key => $value){
                $message .= ' [' . $key .  '=>' . $value . ']';
            }
        }
        Debugger::log($message, $priority);
    }
}