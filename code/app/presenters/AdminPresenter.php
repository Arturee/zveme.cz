<?php
/**
 * Created by PhpStorm.
 * User: Artur
 * Date: 07-Jun-17
 * Time: 10:50 AM
 */
namespace App\Presenters;
use App\Exception\ForbiddenAccessException;
use App\Model\Authenticator;
use App\Model\Configer;
use App\Model\PathFactory;
use App\Model\PosterFactory;
use App\Model\PosterRepository;
use App\Util\DateTime;
use App\Util\Logger;
use Nette\Utils\FileSystem;
use Nette\Utils\Finder;


class AdminPresenter extends BasePresenter {
    /** @var PosterRepository */
    private $posterRepository;
    /** @var PosterFactory */
    private $posterFactory;
    /** @var  string */
    private $sessionDir;
    /** @var  Logger */
    private $logger;

    public function __construct(Configer $configer, PosterRepository $posterRepository,
                                PosterFactory $posterFactory, Logger $logger)
    {
        parent::__construct();
        $this->posterRepository = $posterRepository;
        $this->posterFactory = $posterFactory;
        $this->sessionDir = $configer->getTempDir() . '/sessions';
        $this->logger = $logger;
    }

    public function startup()
    {
        parent::startup();
        /* security */
        if (!$this->getUser()->isInRole(Authenticator::ROLE_ADMIN)){
            throw new ForbiddenAccessException('attack on admin gui');
        }



        // for ajax

//        $this->js->addConfig('url', [
//            'admin-delete-old' => $this->link('Admin:deleteOld')
//        ]);
    }

    public function renderDefault()
    {
        $this->template->sessionFilesCount = $this->getSessionFilesCount();
        $this->template->sessionFilesCountExpired = $this->getExpiredSessionFiles()->count();
        $this->template->posterCount = $this->posterRepository->getPosterCount();
        $this->template->posterCountOld = $this->posterRepository->getOldPosterCount();

        $this->template->timeDb = $this->posterRepository->getTime();
        $this->template->timePhp = DateTime::now()->dbFormat();
        $this->template->postMaxSize = ini_get('post_max_size');
        $this->template->maxInputVars = ini_get('max_input_vars');

        $this->template->writableFolders = $this->pathFactory->createArray([
            '../temp',
            '../log',
            'wall/thumb',
            'wall/hd',
            'temp',
        ]);
    }


    public function actionDeleteAll() : void
    {
        $idsToDelete = $this->posterRepository->getPosterIdsAll();
        $this->deletePosters($idsToDelete);

        $this->logger->logAdminAction('action delete all ' . implode(',', $idsToDelete));
        $this->redirect(':');
    }
    public function actionDeleteOld() : void
    {
        $idsOldPosters = $this->posterRepository->getOldPosterIds();
        $this->deletePosters($idsOldPosters);

        $this->logger->logAdminAction('action delete old ' . implode(',', $idsOldPosters));
        $this->redirect(':');
    }
    public function actionDeleteOldSessionFiles() : void
    {
        $this->deleteExpiredSessionFiles();

        $this->logger->logAdminAction('action delete old sessions');
        $this->redirect(':');
    }


    //AJAX
    public function actionDelete() : void
    {
        /** @var int[] $toDelete */
        $idsToDelete = $this->getRequest()->getPost('toDelete');
        $this->deletePosters($idsToDelete);

        $this->logger->logAdminAction('action delete selected' . implode(',', $idsToDelete));
        $this->payload->status = 'success';
        $this->payload->ids = $idsToDelete;
        $this->sendPayload();
    }
    public function actionLogJsBug() : void
    {
        $request = $this->getRequest();
        if(!$request->isMethod('POST')||!$this->isAjax()){
            $this->logger->logHack(' bad http mehtod for actionJeError');
        }

        $agent = $_SERVER['HTTP_USER_AGENT'];
        $error = $request->getPost('error');
        $text ='JS error:' . $agent . '; ' . $error;
        $this->logger->logJsBug($text);
        $this->terminate();
    }








    /**
     * @param int[] $ids
     */
    private function deletePosters(array $ids) : void
    {
        $this->posterRepository->deletePosters($ids);
        $this->deletePosterImages($ids);
    }
    /**
     * @param int[] $ids
     */
    private function deletePosterImages(array $ids) : void
    {
        foreach ($ids as $id){
            $this->deletePosterImage($id);
        }
    }
    private function deletePosterImage(int $id) : void
    {
        $pathHd = $this->posterFactory->getPathHD($id);
        $pathThumb = $this->posterFactory->getPathThumb($id);
        FileSystem::delete($pathThumb->toLocal());
        FileSystem::delete($pathHd->toLocal());
    }
    private function getSessionFilesCount() : int
    {
        $result = Finder::findFiles('*')
            ->in($this->sessionDir)
            ->count();
        return $result;
    }
    private function getExpiredSessionFiles() : Finder
    {
        $fifteenDaysBack = (new DateTime())->modify('- 15 days');
        //LATER use automatic session OR put this into config
        $result = Finder::findFiles('*')
            ->date('<', $fifteenDaysBack)
            ->in($this->sessionDir);
        return $result;
    }
    private function deleteExpiredSessionFiles() : void
    {
        $expiredFiles = $this->getExpiredSessionFiles();
        foreach($expiredFiles as $file){
            FileSystem::delete($file);
        }
    }
};
