<?php

namespace App\Presenters;
use App\Model\Configer;
use App\Model\PathFactory;
use App\Util\Js;
use App\Util\Logger;
use Kdyby\Translation\Translator;



/**
 * Class BasePresenter
 * @package App\Presenters
 */
class BasePresenter extends \Nette\Application\UI\Presenter {
    //CARE they must be public for @inejct to work
    /** @var Translator @inject */
    public $translator;
    /** @var PathFactory @inject */
    public $pathFactory;
    /** @var Js @inject */
    public $js;
    /** @var Configer @inject */
    public $configer;
    /** @var string @persistent */
    public $locale;
    //kdyby translation needs this
    //persistent stuff must be public

    public function __construct(){
        parent::__construct();
        //we inject services via @inject annotation, so that child Presenters don't have
        //a problem calling parent::__construct()
        $this->locale = 'cs';
        //CARE locale must be set here (the field cannot be directly initialized)
        //because Kdyby\translation param resolver would then not notice it
    }
    /** @Override */
    public function startup(){
        parent::startup();

        //$this->js->setMinify();
        $this->js->addConfig('url', [
            'bugtracker' => $this->link('Admin:logJsBug'),
            'homepage' => $this->link('Homepage:')
        ]);
        $this->js->addConfig('conf', [
            'debugmode' => $this->configer->isDebugMode()
        ]);
        $this->js->addLibs(
            $this->pathFactory->createArray([
                'bower_components/jQuery/dist/jquery.js',
                'bower_components/i18n.min/index.js',
                'bower_components/bootstrap/dist/js/bootstrap.js',
                'bower_components/nette.ajax.js/nette.ajax.js',
                'bower_components/live-form-validation/index.js'
            ]));
        $this->js->addFolders(
            $this->pathFactory->createArray([
                'js/exceptions',
                'js',
            ]));
        $this->template->js = $this->js;



    }


    /**
     * @param string $word
     * @return string
     */
    public function i18n($word){
        return $this->translator->translate($word);
    }
}