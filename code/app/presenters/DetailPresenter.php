<?php
/**
 * Created by PhpStorm.
 * User: Artur
 * Date: 18-Jul-17
 * Time: 7:46 PM
 */


namespace App\Presenters;
use App\Model\PosterRepository;


class DetailPresenter extends BasePresenter
{
    /** @var PosterRepository */
    private $posterRepository;

    public function __construct(PosterRepository $posterRepository)
    {
        $this->posterRepository = $posterRepository;
    }

    public function startup()
    {
        parent::startup();
        $this->js->addLibs(
            $this->pathFactory->createArray([
                'bower_components/magnific-popup/dist/jquery.magnific-popup.js'
        ]));
        $this->js->addFiles(
            $this->pathFactory->createArray([
                'js/detail/role/guest.js'
        ]));
    }

    public function renderDefault(int $posterId)
    {
        $this->template->poster = $this->posterRepository->get($posterId);
        //getPathHd
        //getCategory
        //getUrl
        //getDateEnd
        //getName
        //getDescription
        //getLocation
    }
}