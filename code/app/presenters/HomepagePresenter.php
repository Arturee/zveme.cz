<?php

namespace App\Presenters;
use App\Control\SigninControlFactory;
use App\Model\CategoryRepository;
use App\Model\PosterRepository;
use App\Control\SigninControl;
use Nette\Mail\IMailer;
use App\Util\Logger;
use App\Model\Authenticator;


/**
 *
 * Class HomepagePresenter
 * @package App\Presenters
 */
class HomepagePresenter extends BasePresenter {
    /** @var Logger */
    private $logger;
    /** @var PosterRepository */
    private $PosterRepository;
    /** @var CategoryRepository */
    private $categoryRepository;
    /** @var SigninControlFactory */
    private $signInControlFactory;

    public function __construct(PosterRepository $PosterRepository, IMailer $mailer,
                                CategoryRepository $categoryRepository, Logger $logger,
                                SigninControlFactory $signInControlFactory)
    {
        parent::__construct();
        $this->PosterRepository = $PosterRepository;
        $this->categoryRepository = $categoryRepository;
        $this->logger = $logger;
        $this->signInControlFactory = $signInControlFactory;
    }
    /** @Override */
    public function startup()
    {
        parent::startup();

        $this->js->addLibs(
            [ $this->pathFactory->create('bower_components/jquery.cookie/jquery.cookie.js') ]
        );
        $this->js->addFolders(
            $this->pathFactory->createArray([
                'js/home/ui'
            ])
        );


        if ($this->getUser()->isInRole(Authenticator::ROLE_ADMIN)){

            $this->js->addFiles([
                $this->pathFactory->create('js/home/role/admin.js')
            ]);
            $this->js->addConfig('url', [
                'deletePosters'=> $this->link('Admin:delete')
            ]);

        } else {
            $this->js->addFiles([
                $this->pathFactory->create('js/home/role/guest.js')
            ]);
        }

    }

    public function renderDefault() : void
    {
        $this->template->version = 'v0.1';
        $this->template->posters = $this->PosterRepository->getPostersVisible();
        $this->template->categories = $this->categoryRepository->getAllNonEmpty();
    }


    /**
     * @return SigninControl
     */
    protected function createComponentSignin() : SigninControl
    {
        $signInControl = $this->signInControlFactory->create();
        $presenter = $this;
        $signInControl->onLogin[] = function($control)use($presenter){
            //$presenter->flashMessage('Login callback');
            $presenter->redirect('Homepage:default');
        };
        $signInControl->onLogout[] = function($control)use($presenter){
            //$presenter->flashMessage('Logout callback');
            $presenter->redirect('Homepage:default');
        };
        return $signInControl;
    }
}














// presenter lifecycle

//    protected function afterRender(){
//        parent::afterRender();
//    }
//    public function shutdown($response){
//        parent::shutdown($response);
//    }




//    public function actionDefault() : void {
//    }
//protected function beforeRender(){
//parent::beforeRender();
//}







// session
//
//        $session = $this->getSession();
//        Debugger::barDump($session->isStarted());
//        Debugger::barDump($session->getId());
//
//        $section = $this->getSession('auto');
//        if ($section->znacka){
//            Debugger::barDump("znacka nastavena");
//        } else {
//            Debugger::barDump("znacka chybi. nastavuji znovu");
//            $section->znacka = "Alfa Romeo";
//        }
//
//
//        $session->destroy();
//        Debugger::barDump($session->isStarted());



//check user's privileges
/*
 * Presenter
 *
$this->getUniqueId();
$this->getRequest();
$this->getHttpRequest();
$this->getPayload();
$this->getSession();
$this->getUser();
*
 * Component
$this->getParameters();
$this->getPersistentParams();
*
 * HTTP request (should not be touched!)
 *
$this->getHttpRequest()->getQuery();
$this->getHttpRequest()->getPost();
$this->getHttpRequest()->getFile();
$this->getHttpRequest()->getHeader();
$this->getHttpRequest()->isSecured();
$this->getHttpRequest()->isAjax();
$this->getHttpRequest()->isMethod();
$this->getHttpRequest()->getRemoteAddress();
 *
 * Nette application request
 *
 *
$this->getRequest()->setPresenterName();
$this->getRequest()->setParameters();
$this->getRequest()->setPost();
$this->getRequest()->setFiles();
$this->getRequest()->setMethod();
$this->getRequest()->setFlag();
 * Detail:default 1, lang=>cn - translate to getParameters() - translates to GET params
 *
 *
 *
 *
*/
//$this->terminate();