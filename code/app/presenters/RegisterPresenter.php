<?php
/**
 * Created by PhpStorm.
 * User: Artur
 * Date: 05-Jul-17
 * Time: 5:05 PM
 */

namespace App\Presenters;
use App\Control\EnhancedForm;
use App\Exception\EmailAlreadyRegisteredException;
use App\Exception\NameAlreadyRegisteredException;
use App\Model\UserRepository;
use Nette\Application\UI\Form;
use Nette\Utils\Strings;


class RegisterPresenter extends BasePresenter {
    /** @var UserRepository */
    private $userManager;

    public function __construct(UserRepository $userManager){
        parent::__construct();
        $this->userManager = $userManager;
    }

    public function renderDefault(){

    }

    public function createComponentRegistrationForm(){
        $form = new EnhancedForm($this->translator);
        $form->addText('username', '')
            ->setRequired('ui.rg.required.username');
        $form->addText('email', '')
            ->addRule(Form::EMAIL, 'ui.rg.invalid.email')
            ->setRequired('ui.rg.required.email');
        $form->addPassword('password', '')
            ->setRequired('ui.rg.required.password');
        $form->addPassword('passwordCheck', '')
            ->setRequired('ui.rg.required.password2')
            ->addRule(Form::EQUAL, 'ui.rg.passwordsDontMatch', $form['password']);
        $form->addSubmit('register', 'ui.rg.register');
        $form->onSuccess[] = [$this, 'successHandler'];
        return $form;

        //$form->onSuccess[]
        //$form->onError[]
        //$form->onSubmit[]
        //$form->onValidate[]
    }

    public function successHandler(Form $form, $values){
        $name = Strings::trim($values['username']);
        $email = Strings::trim($values['email']);
        $password = Strings::trim($values['password']);
        try {

            $this->userManager->insertUser($name, $email, $password);
            $this->flashMessage($this->i18n('ui.rg.success'));
            $this->redirect('Homepage:');
        } catch(EmailAlreadyRegisteredException $e){
            $form->addError($this->i18n('ui.rg.alreadyRegistered.email'));
        } catch(NameAlreadyRegisteredException $e){
            $form->addError($this->i18n('ui.rg.alreadyRegistered.name'));
            //LATER nicer styling for the error
        }
    }
}
