<?php
/**
 * Created by PhpStorm.
 * User: Artur
 * Date: 07-Jun-17
 * Time: 10:50 AM
 */
namespace App\Presenters;

use App\Control\EnhancedForm;
use App\Model\PaymentManager;
use Nette\Application\UI\Form;
use App\Model\PosterRepository;
use App\Model\PosterUploader;
use App\Model;
use Nette\Utils\Validators;
use Nette\Http;
use App\Model\CategoryRepository;
use App\Model\Upload;
use App\Util\Logger;
use Nette\Utils\DateTime;
use App\Exception\HackException;
use App\Exception\FailureError;



/**
 * LATER more rigorous anti-hack checks
 *
 * Class UploadPresenter
 * @package App\Presenters
 */
class UploadPresenter extends BasePresenter {
    /**
     * @var PosterRepository
     */
    private $PosterRepository;
    /**
     * @var PaymentManager
     */
    private $paymentManager;
    /**
     * @var PosterUploader
     */
    private $posterUploader;
    /**
     * @var Http\SessionSection
     */
    private $sectionUpload;
    /**
     * @var Logger
     */
    private $logger;
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;
    

    public function __construct(Http\Session $session, PosterRepository $PosterRepository,
                                PosterUploader $posterUploader, PaymentManager $paymentManager,
                                CategoryRepository $categoryRepository, Logger $logger)
    {
        parent::__construct();
        $this->PosterRepository = $PosterRepository;
        $this->categoryRepository = $categoryRepository;
        $this->posterUploader = $posterUploader;
        $this->sectionUpload = $session->getSection('upload');
        $this->paymentManager = $paymentManager;
        $this->logger = $logger;
    }

    /** @Override */
    public function startup()
    {
        parent::startup();

        $this->js->addConfig('url', [
            'uploadChunk' => $this->link('Upload:nextChunk')
        ]);
        $this->js->addLibs(
            [ $this->pathFactory->create('/bower_components/jquery-ui/jquery-ui.min.js') ]
        );
        $this->js->addFolders(
            $this->pathFactory->createArray([
                'js/upload',
                'js/upload/ui',
            ])
        );
        $this->js->addFiles(
            [ $this->pathFactory->create('js/upload/role/guest.js') ]
        );
    }
    protected function createComponentUploadForm() : Form
    {
        $form = new EnhancedForm($this->translator);
        $form->addText('fakeEndDate')
            ->setRequired('ui.up.form.required.date');
        $form->addHidden('endDate')
            ->setRequired();

        $categories = $this->categoryRepository->getAll();

        $form->addSelect('categoryId', '', $categories);
            //Label texts are supplied manually, so there is no need for them here
        $form->addText('categoryNew');
        $form->addText('location');
        $form->addText('url');
        $form->addText('name');
        $form->addTextArea('description', '');
        $form->addSubmit('upload', 'ui.up.form.submit');
        $form->addHidden('sizeGoal')->setRequired();
        $form->addHidden('thumbDataUrl')->setRequired();
        $form->onSuccess[] = [$this, 'initiateUpload'];
        $form->onError[] = function(Form $sender){
            //onError() Occurs when the form is submitted and is not valid
            //This should not happen thanks to fornt-end validation, so it is a hack attempt
            $this->logger->logHack('Upload: - guest form contains invalid data');
        };
        return $form;
    }
    public function initiateUpload(Form $sender, $values) : void
    {
        //CARE values are not typed
        $ownerId = null;
        if ($this->getUser()->isLoggedIn()){
            $ownerId = $this->getUser()->getId();
        }
        $endDate = DateTime::from($values->endDate);
        $upload = new Upload(
            $endDate,
            $values->categoryId,
            $values->categoryNew,
            $values->thumbDataUrl,
            $values->location,
            $values->url,
            $values->name,
            $values->description,
            (int)$values->sizeGoal,
            $ownerId
        );
        $this->posterUploader->registerNewUpload($upload);
        //ajax response will have an empty body
    }
    public function actionNextChunk()
    {
        //careful validation
        $request = $this->getRequest();
        if(!$request->isMethod('POST') || !$this->isAjax()){
            $this->terminateWithHackError('Upload:nextChunk not via AJAx or not POST method');
        }

        $id = $request->getPost('id');
        $size = $request->getPost('size');
        $imageHdDataUrlPart = $request->getPost('hdDataUrlPart');

        if ($id===null || !$imageHdDataUrlPart || Validators::is($size, 'int')){
            $data = new \stdClass();
            $data->id = $id;
            $data->dataUrl = $imageHdDataUrlPart;
            $data->size = $size;
            $this->terminateWithHackError('Upload:nextChunk bad data', $data);
        }



        $size = (int)$size;
        try {
            $upload = $this->posterUploader->receiveNextChunk($id, $size, $imageHdDataUrlPart);
            $this->send($upload);
        } catch(HackException $e){
            $this->terminateWithHackError($e->getMessage());
        } catch(FailureError $e){
            $this->logger->logHack($e->getMessage());
            $this->getHttpResponse()->setCode(Http\IResponse::S500_INTERNAL_SERVER_ERROR);
            $this->terminate();
        }

        //POST method is used instead of a more fitting PUT method
        //simply because the support for PUT method in PHP is quite bad
        //$x  = file_get_contents('php://input');
            //returns id=42&chunkSize=123214&data=[html encoded string]
    }




















    private function terminateWithHackError(string $message, $data = null) : void
    {
        $this->logger->logHack($message, $data);
        $this->getHttpResponse()->setCode(Http\IResponse::S400_BAD_REQUEST);
        $this->terminate();
    }
    private function send(Upload $upload) : void
    {
        $this->payload->status = $upload->getStatus();
        $this->payload->id = $upload->getId();
        $this->payload->size = $upload->getSize();
        $this->sendPayload();
    }


















    //LATER
    public function actionPayment($posterId){
        if (!$this->isAjax()){
            //LATER or bad method check
            //LATER error
            return;
        }
        /** @var $poster  Model\Poster */
        $poster = $this->sectionUpload->poster;
        if (!$poster){
            //LATER error
            return;
        }
        if (!$this->getUser()->isLoggedIn()){
            //LATER error
            return;
            //LATER velkej pozor na hackovani
        }
        //if session is not hijacked, we should be ok?.
        //LATER pozor na opakovany requesty - zakazat a dat spinner

        $AMOUNT = 100;
        $poster->setPayment($AMOUNT);
        $this->PosterRepository->registerPayment($poster);
        $this->paymentManager->insertPayment($poster->getOwnerId(), $poster->getId(), $poster->getPayment());

        //LATER zrusit pay possibility hned jak user prejde zpet na homepage = promaznout session
        $this->terminate();
    }
}





//    /**
//
//     * @return Form
//     */
//    protected function createComponentUploadFormAdmin() {
//        //in latte use {control uploadForm}
//        $form = new Form;
//        $form->addSelect('category', 'Category', ['cinema', 'sport', 'music', 'sexy']);
//        $multipleTrue = true;
//        $form->addUpload('poster', 'Your poster:', $multipleTrue)
//            ->setRequired(FALSE) //it seems that files must have this specified in nette
//            ->addRule(Form::IMAGE, 'image format must be JPEG, PNG or GIF')
//            ->addRule(Form::MAX_FILE_SIZE, 'file size maximum 10 MB', 10*2014*1024);
//        //These will be applied only in js, just before submit all files are erased
//        //because the form is handled by AJAX
//        $form->addSubmit('upload', 'Upload')->setDisabled();
//        //this form never submits, AJAX is used instead
//        return Boostrap3Form::enhance($form);
//    }