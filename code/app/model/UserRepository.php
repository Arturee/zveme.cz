<?php

namespace  App\Model;
use App\Exception\EmailAlreadyRegisteredException;
use App\Exception\NameAlreadyRegisteredException;
use App\Exception\UserAlreadyRegisteredException;
use Nette;
use Nette\Database;
use Nette\Security\Passwords;
use Nette\Utils\Strings;

/**
 *
 * Created by PhpStorm.
 * User: Artur
 * Date: 08-May-17
 * Time: 8:12 PM
 */
class UserRepository {
    use Nette\SmartObject;

    const
        TABLE_USERS = 'users',
        EMAIL = 'email',
        LOGIN = 'login',
        PASSWORD = 'password',
        ROLE = 'role',
        ID = 'id';

    /**
     * @var Database\Context
     */
    private $database;

    public function __construct(Database\Context $database)
    {
        $this->database = $database;
    }
    /**
     * @param string $name
     * @param string $email
     * @param string $passwordText
     * @throws EmailAlreadyRegisteredException
     * @throws NameAlreadyRegisteredException
     */
    public function insertUser(string $name, string $email, string $passwordText) : void
    {
        $this->database->beginTransaction();

        $emailAlreadyInDb = $this->database->table(self::TABLE_USERS)
            ->where(self::EMAIL, $email)
            ->fetch();
        if ($emailAlreadyInDb) {
            $this->database->rollBack();
            throw new EmailAlreadyRegisteredException('');
        }

        $nameAlreadyInDb = $this->database->table(self::TABLE_USERS)
                ->where(self::LOGIN, $name)
                ->fetch();
        if ($nameAlreadyInDb){
            $this->database->rollBack();
            throw new NameAlreadyRegisteredException('');
        }


        $password = Passwords::hash($passwordText);
        $this->database->table(self::TABLE_USERS)->insert([
            self::LOGIN=>$name,
            self::EMAIL=>$email,
            self::PASSWORD=>$password
        ]);
        $this->database->commit();
    }
    public function getWrap(string $username) : WrapUserPassword
    {
        $row = $this->database->table(self::TABLE_USERS)
            ->where(self::LOGIN, $username)
            ->fetch();
        if ($row){
            $result = self::createWrap($row);
        } else {
            $result = null;
        }
        return $result;
    }
    public function get(int $id) : User
    {
        $row = $this->database->table(self::TABLE_USERS)
            ->where(self::ID, $id)
            ->fetch();
        if ($row){
            $result = self::createUser($row);
        } else {
            $result = null;
        }
        return $result;
    }









    private static function createUser(Database\IRow $row) : User
    {
        $user = new User(
            $row->offsetGet(self::ID),
            $row->offsetGet(self::LOGIN),
            $row->offsetGet(self::EMAIL),
            $row->offsetGet(self::ROLE)
        );
        return $user;
    }
    private static function createWrap(Database\IRow $row) : WrapUserPassword
    {
        $user = self::createUser($row);
        $password = $row->offsetGet(self::PASSWORD);
        return new WrapUserPassword($user, $password);
    }

}