<?php declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: Artur
 * Date: 05-Jul-17
 * Time: 9:07 PM
 *
 * LATER
 *
 */

namespace App\Model;
use Nette;
use Nette\Database;


class PaymentManager {
    use Nette\SmartObject;
    /**
     * @var Database\Context
     */
    private $database;

    public function __construct(Database\Context $database){
        $this->database = $database;
    }
    /**
     * @param $userId int
     * @param $posterId int
     * @param $payment int
     */
    public function insertPayment($userId, $posterId, $payment) : void {
        $this->database->table('payments')->insert([
            'user_id'=>$userId,
            'poster_id'=>$posterId,
            'payment'=>$payment
        ]);
    }
}