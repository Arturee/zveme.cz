<?php
/**
 * Created by PhpStorm.
 * User: Artur
 * Date: 13-Sep-17
 * Time: 12:36 PM
 */

namespace App\Model;


class User
{
    /** @var  int */
    private $id;
    /** @var  string */
    private $name, $email, $role;

    /**
     * User constructor.
     * @param int $id
     * @param string $name
     * @param string $email
     * @param string $role
     */
    public function __construct(int $id, string $name, string $email, string $role)
    {
        $this->id = $id;
        $this->name = $name;
        $this->email = $email;
        $this->role = $role;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getRole(): string
    {
        return $this->role;
    }
}