<?php

namespace App\Model;
use App\Exception\CannotWriteToFileError;
use Nette\Http;
use Nette;
use App\Exception\HackException;

/**
 * Class PosterUploader
 * @package App\Model
 */
class PosterUploader {
    use Nette\SmartObject;

    /** @var Http\SessionSection */
    private $sectionUpload;
    /** @var PosterRepository */
    private $PosterRepository;
    /** @var  PathFactory */
    private $pathFactory;


    public function __construct(Http\Session $session, PosterRepository $PosterRepository, PathFactory $pathFactory)
    {
        $this->sectionUpload = $session->getSection('upload');
        //gets created automatically if it doesn't exists yet
        $this->PosterRepository = $PosterRepository;
        $this->pathFactory = $pathFactory;
    }
    /**
     * @param Upload $upload
     */
    public function registerNewUpload(Upload $upload) : void {
        $this->setUpload($upload);
    }
    /**
     *
     *
     * @param $communicationID
     * @param $chunkSize
     * @return Upload
     * @throws HackException
     * @throws CannotWriteToFileError
     */
    public function receiveNextChunk($communicationID, $chunkSize, $dataUrlPart) : Upload
    {
        $upload = $this->getUpload();
        if (!$upload){
            throw new HackException('not started - on upload in session');
        }
        $path = $this->tempPath($upload);
        $writtenOK = @file_put_contents($path, $dataUrlPart, FILE_APPEND);
        if (!$writtenOK){
            throw new CannotWriteToFileError($path);
        }
        $upload->increaseSize($chunkSize);
        if ($upload->isComplete()){

            $dataUrlHd = file_get_contents($path);
            unlink($path);
            $poster = $this->PosterRepository->insertPoster($upload, $dataUrlHd);
            $this->sectionUpload->poster = $poster;
            $upload->setStatus('complete');

        } else {
            $upload->setStatus('progress');
        }
        return $upload;
    }



    
    




    private function getUpload() : Upload {
        return $this->sectionUpload->upload;
    }
    private function setUpload(Upload $upload) : void
    {
        $this->sectionUpload->upload = $upload;
    }
    private function tempPath(Upload $upload) : string
    {
        $TEMP_DIR = 'temp';
        return $this->pathFactory->create($TEMP_DIR)
            ->append($upload->getId())
            ->toLocal();
    }
}