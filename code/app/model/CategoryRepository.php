<?php

namespace  App\Model;
use Nette;
use Nette\Database;
use Nette\Database\UniqueConstraintViolationException;


class CategoryRepository {
    use Nette\SmartObject;
    const
        TABLE_CATEGORIES = 'categories',
        ID = 'id',
        TEXT = 'text';

    /** @var Database\Context */
    private $database;

    public function __construct(Database\Context $database)
    {
        $this->database = $database;
    }
    /**
     * @param string $category
     * @return int category id
     */
    public function insertCategory(string $category) : int
    {
        /**
         * @var Nette\Database\IRow
         */
        $row = null;
        try {
            $row = $this->database->table(self::TABLE_CATEGORIES)
                ->insert([
                    self::TEXT=>$category
                ]);
        } catch (UniqueConstraintViolationException $e){
            $row = $this->database->table(self::TABLE_CATEGORIES)
                ->where(self::TEXT, $category)
                ->fetch();
        }
        $id = $row->offsetGet(self::ID);
        return $id;
    }
    public function get(int $id) : string
    {
        $row = $this->database->table(self::TABLE_CATEGORIES)->get($id);
        $id = $row->offsetGet(self::ID);
        return $id;
    }
    /**
     * @return string[] associative array
     */
    public function getAll() : array
    {
        $selection = $this->database->table(self::TABLE_CATEGORIES)->fetchAll();
        $result = self::createAssociativeArray($selection);
        return $result;
    }
    /**
     * @return string[] associative array
     */
    public function getAllNonEmpty() : array
    {
        $subQuery = "(SELECT " . PosterRepository::CATEGORY_ID . " FROM " . PosterRepository::TABLE_POSTERS .")";
        $query = "SELECT * FROM " . self::TABLE_CATEGORIES . " WHERE " . self::ID . " IN " . $subQuery;
        $selection = $this->database->query($query);
        $result = self::createAssociativeArray($selection);
        return $result;
    }








    /**
     * @param Database\ResultSet | Database\IRow[] $selection
     * @return string[] associative array
     */
    private static function createAssociativeArray($selection) : array
    {
        $result = [];
        foreach ($selection as $row){
            $id = $row->offsetGet(self::ID);
            $category = $row->offsetGet(self::TEXT);
            $result[$id] = $category;
        }
        return $result;
    }
}