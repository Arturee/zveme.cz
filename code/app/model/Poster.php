<?php declare(strict_types=1);


/**
 * Created by PhpStorm.
 * User: Artur
 * Date: 06-May-17
 * Time: 11:43 PM
 */
namespace App\Model;
use Nette;
use App\Util\Path;
use Nette\Utils\DateTime;


class Poster {
    use Nette\SmartObject;

    /** @var  int */
    private $id, $ownerId, $payment;
    /** @var  Path */
    private $pathHd, $pathThumb;
    /** @var  DateTime */
    private $dateUploaded, $dateEnd;
    /** @var  string */
    private $category, $url, $name, $description, $location;

    
    
    /**
     * Poster constructor.
     * @param int $id
     * @param int $ownerId
     * @param int $payment
     * @param Path $pathHd
     * @param Path $pathThumb
     * @param DateTime $dateUploaded
     * @param DateTime $dateEnd
     * @param string $category
     * @param string $url
     * @param string $name
     * @param string $description
     * @param string $location
     */
    public function __construct($id, $ownerId, $payment, Path $pathHd, Path $pathThumb,
                                DateTime $dateUploaded, DateTime $dateEnd, $category, $url,
                                $name, $description, $location)
    {
        $this->id = $id;
        $this->ownerId = $ownerId;
        $this->payment = $payment;
        $this->pathHd = $pathHd;
        $this->pathThumb = $pathThumb;
        $this->dateUploaded = $dateUploaded;
        $this->dateEnd = $dateEnd;
        $this->category = $category;
        $this->url = $url;
        $this->name = $name;
        $this->description = $description;
        $this->location = $location;
    }


    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getOwnerId() : int
    {
        return $this->ownerId;
    }

    /**
     * @return int
     */
    public function getPayment() : int
    {
        return $this->payment;
    }

    /**
     * @return Path
     */
    public function getPathHd() : Path
    {
        return $this->pathHd;
    }

    /**
     * @return Path
     */
    public function getPathThumb() : Path
    {
        return $this->pathThumb;
    }

    /**
     * @return DateTime
     */
    public function getDateUploaded() : DateTime
    {
        return $this->dateUploaded;
    }

    /**
     * @return string
     */
    public function getCategory() : string
    {
        return $this->category;
    }
    /**
     * @return DateTime
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }


}