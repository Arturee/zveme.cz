<?php declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: Artur
 * Date: 06-Jul-17
 * Time: 11:24 AM
 */

namespace App\Model;
use Nette;
use Nette\Utils\DateTime;
use App\Util\Path;


class PosterFactory {
    use Nette\SmartObject;

    /** @var PathFactory */
    private $pathFactory;


    /**
     * PosterFactory constructor.
     * @param PathFactory $pathFactory
     */
    public function __construct(PathFactory $pathFactory)
    {
        $this->pathFactory = $pathFactory;
    }

    /**
     * @param $id
     * @param $ownerId
     * @param $payment
     * @param DateTime $dateUploaded
     * @param DateTime $dateEnd
     * @param $category
     * @param $url
     * @param $name
     * @param $description
     * @param $location
     * @return Poster
     */
    public function create($id, $ownerId, $payment, $dateUploaded, $dateEnd, $category, $url, $name,
                           $description, $location) : Poster
    {
        $pathHd = $this->getPathHD($id);
        $pathThumb = $this->getPathThumb($id);
        return new Poster($id, $ownerId, $payment, $pathHd, $pathThumb, $dateUploaded, $dateEnd,
            $category, $url, $name, $description, $location);
    }


    public function getPathHD(int $id) : Path
    {
        $pathHd = 'wall/hd/' . $id . '.png';
        $pathHd = $this->pathFactory->create($pathHd);
        return $pathHd;
    }

    public function getPathThumb(int $id) : Path
    {
        $pathHd = 'wall/thumb/' . $id . '.png';
        $pathHd = $this->pathFactory->create($pathHd);
        return $pathHd;
    }
}