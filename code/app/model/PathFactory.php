<?php

/**
 * Created by PhpStorm.
 * User: Artur
 * Date: 06-Jul-17
 * Time: 10:00 AM
 */

namespace App\Model;
use App\Util\Path;
use Nette;

class PathFactory {
    use Nette\SmartObject;
    /** @var string */
    private $wwwDir;

    public function __construct(string $wwwDir)
    {
        $this->wwwDir = $wwwDir;
    }
    public function create(string $pathString) : Path
    {
        return new Path($this->wwwDir, $pathString);
    }
    /**
     * @param string[] $pathStrings
     * @return Path[]
     */
    public function createArray(array $pathStrings) : array
    {
        /** @var $result Path[] */
        $result = [];
        foreach ($pathStrings as $pathString){
            $result[] = $this->create($pathString);
        }
        return $result;
    }
    /**
     * @param Path[] $paths
     * @return string[]
     */
    public static function pathsToAbsolutePathStringArray(array $paths) : array
    {
        $result = [];
        foreach ($paths as $path){
            $result[] = $path->toLocal();
        }
        return $result;
    }
}