<?php declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: Artur
 * Date: 06-Jul-17
 * Time: 9:51 AM
 */

namespace App\Model;
use Nette;

/**
 * Class Configer
 * Should only be used in a base presenter.
 *
 * @package App\Model
 */
class Configer {
    use Nette\SmartObject;
    /** @var string */
    private $appDir, $wwwDir, $tempDir;
    /** @var bool */
    private $debugMode, $productionMode, $consoleMode;

    public function __construct($appDir, $wwwDir, $debugMode, $productionMode, $consoleMode, $tempDir){
        $this->appDir = $appDir;
        $this->wwwDir = $wwwDir;
        $this->debugMode = $debugMode;
        $this->productionMode = $productionMode;
        $this->consoleMode = $consoleMode;
        $this->tempDir = $tempDir;
    }
    /**
     * @return string
     */
    public function getAppDir() : string {
        return $this->appDir;
    }
    /**
     * @return string
     */
    public function getTempDir() : string {
        return $this->tempDir;
    }
    /**
     * @return string
     */
    public function getWwwDir() : string {
        return $this->wwwDir;
    }
    /**
     * @return boolean
     */
    public function isDebugMode() : bool {
        return $this->debugMode;
    }
    /**
     * @return boolean
     */
    public function isProductionMode() : bool {
        return $this->productionMode;
    }
    /**
     * @return boolean
     */
    public function isConsoleMode() : bool {
        return $this->consoleMode;
    }
}