<?php
/**
 * Created by PhpStorm.
 * User: Artur
 * Date: 13-Sep-17
 * Time: 12:42 PM
 */

namespace App\Model;


class WrapUserPassword
{
    /**
     * @var User
     */
    private $user;
    /**
     * @var string
     */
    private $password;

    /**
     * WrapUserPassword constructor.
     * @param User $user
     * @param string $password
     */
    public function __construct(User $user, $password)
    {
        $this->user = $user;
        $this->password = $password;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }
}