<?php

namespace  App\Model;
use App\Exception\CannotWriteToFileError;
use Nette;
use Nette\Database;
use App\Util\Path;
use Nette\Database\Table\ActiveRow;
use Nette\Utils\DateTime;


/**
 *
 * Created by PhpStorm.
 * User: Artur
 * Date: 08-May-17
 * Time: 8:12 PM
 */
class PosterRepository {
    use Nette\SmartObject;

    const
        TABLE_POSTERS = 'posters',

        ID = 'id',
        PAYMENT = 'payment',
        END_DATE = 'date_end',
        CATEGORY_ID = 'id_category',
        OWNER_ID = 'id_owner',
        LOCATION = 'location',
        URL = 'url',
        NAME = 'name',
        DESCRIPTION = 'description',
        DELETED = 'deleted',
        DATE_UPLOADED = 'date_uploaded',

        TABLE_CATEGORIES = 'categories',

        CATEGORIES_TEXT = 'text',
        CATEGORIES_ID = 'id';


    /** @var Database\Context */
    private $database;
    /** @var  PosterFactory */
    private $posterFactory;
    /** @var  CategoryRepository */
    private $categoryRepository;


    public function __construct(Database\Context $database, PosterFactory $posterFactory,
                                CategoryRepository $categoryRepository)
    {
        $this->database = $database;
        $this->posterFactory = $posterFactory;
        $this->categoryRepository = $categoryRepository;
    }
    /**
     * @param Upload $upload
     * @param string $dataUrlHd
     * @return Poster
     */
    public function insertPoster(Upload $upload, $dataUrlHd) : Poster
    {
        $categoryId = $upload->getCategoryId();
        if ($upload->getCategoryNew()){
            $possibleNewCategory = $upload->getCategoryNew();
            $id = $this->categoryRepository->insertCategory($possibleNewCategory);
            $categoryId = $id;
        }

        $activeRow = $this->database->table(self::TABLE_POSTERS)->insert([
            self::END_DATE=>$upload->getEndDate(),
            self::CATEGORY_ID=>$categoryId,
            self::OWNER_ID=>$upload->getOwnerId(),
            self::LOCATION=>$upload->getLocation(),
            self::URL=>$upload->getUrl(),
            self::NAME=>$upload->getName(),
            self::DESCRIPTION=>$upload->getDescription()
        ]);
        $poster = $this->createPoster($activeRow);

        //save images
        $this->savePoster($poster, $upload->getThumbDataUrl(), $dataUrlHd);
        return $poster;
    }
    /**
     * @return Poster[]
     */
    public function getPostersVisible() : array
    {
        $now = new DateTime();
        $selection = $this->database->table(self::TABLE_POSTERS)
            ->where(self::DELETED, false)
            ->where(self::END_DATE . ' >= ?', $now)
            ->order(self::END_DATE . ' ASC');
            //->where('id', [269, 270, 271, 272, 273]);
            //->limit(5);
        $result = [];
        foreach($selection as $activeRow){
            $result[] = $this->createPoster($activeRow);
        }
        return $result;
    }
    /**
     * @return int[]
     */
    public function getPosterIdsAll() : array
    {
        $selection = $this->database->table(self::TABLE_POSTERS);
        $result = [];
        foreach($selection as $row){
            $result[] = $row->offsetGet(self::ID);
        }
        return $result;
    }
    /**
     * @param int $id
     * @return Poster
     */
    public function get(int $id) : Poster
    {
        $row = $this->database->table(self::TABLE_POSTERS)->get($id);
        return $this->createPoster($row);
    }
    /**
     * @param $ids int[]|int
     */
    public function deletePosters($ids) : void
    {
        $this->database->table(self::TABLE_POSTERS)->where('id', $ids)->delete();
    }
    /**
     * @return int[]
     */
    public function getOldPosterIds() : array
    {
        $now = new DateTime();
        $rows = $this->getTable()->where(self::END_DATE . ' < ?', $now)->fetchAll();
        $ids = [];
        foreach ($rows as $row){
            $ids[] = $row->offsetGet(self::ID);
        }
        return $ids;
    }
    public function getPosterCount() : int
    {
        return $this->getTable()->count();
    }
    public function getOldPosterCount() : int
    {
        return sizeof($this->getOldPosterIds());
    }


    //LATER move elsewhere
    public function getTime() : DateTime
    {
        /** @var DateTime $time */
        $time = $this->database->query('SELECT NOW() AS time')->fetch()->time;
        return $time;
    }















    /**
     * @Deprecated
     */
    public function manualInsertPoster($id, Upload $upload)
    {
        $activeRow = $this->database->table(self::TABLE_POSTERS)->insert([
            self::ID=>$id,
            self::END_DATE=>$upload->getEndDate(),
            self::CATEGORY_ID=>$upload->getCategoryId(),
            self::OWNER_ID=>$upload->getOwnerId(),
            self::LOCATION=>$upload->getLocation(),
            self::URL=>$upload->getUrl(),
            self::NAME=>$upload->getName(),
            self::DESCRIPTION=>$upload->getDescription()
        ]);
    }
    /**
     * LATER
     * @param Poster $poster
     */
    public function registerPayment(Poster $poster){
        //check that the poster IS actually in the DB
        
        $this->database->table(self::TABLE_POSTERS)
            ->where('id', $poster->getId())
            ->update([
                'payment'=>$poster->getPayment()
            ]);
        //get() cannot be used here, because it returns IRow and we neet Selection
    }









  




    private function getTable(){
        return $this->database->table(self::TABLE_POSTERS);
    }
    /**
     * @param ActiveRow $activeRow
     * @return Poster
     */
    private function createPoster(Database\IRow $activeRow) : Poster {
        $id='id';
        $ownerId = self::OWNER_ID;
        $payment = self::PAYMENT;
        $dateUploaded = self::DATE_UPLOADED;
        $endDate = self::END_DATE;
        $categoryId = self::CATEGORY_ID;
        $url = self::URL;
        $name = self::NAME;
        $description = self::DESCRIPTION;
        $location = self::LOCATION;
        //$deleted = self::DELETED;
        $category = $this->categoryRepository->get($activeRow->$categoryId); //LATER more efficiently!

        return $this->posterFactory->create(
            $activeRow->$id,
            $activeRow->$ownerId,
            $activeRow->$payment,
            $activeRow->$dateUploaded,
            $activeRow->$endDate,
            $category,
            $activeRow->$url,
            $activeRow->$name,
            $activeRow->$description,
            $activeRow->$location
        );
    }





    /**
     * LATER maybe move these out - they do not belong to a repository class really
     *
     * @param Poster $poster
     * @param string $dataUrlThumb
     * @param string $dataUrlHD
     */
    private function savePoster(Poster $poster, string $dataUrlThumb, string $dataUrlHD) : void
    {
        $this->savePNG($dataUrlThumb, $poster->getPathThumb());
        $this->savePNG($dataUrlHD, $poster->getPathHd());
    }
    /**
     * @param string $dataUrl
     * @param Path $path
     * @throws CannotWriteToFileError
     */
    private function savePNG(string $dataUrl, Path $path) : void
    {
        list($typeAndEncoding, $base64Data) = explode(',', $dataUrl);
        $binaryData = base64_decode($base64Data);
        $writtenOK = @file_put_contents($path->toLocal(), $binaryData);
        if (!$writtenOK){
            throw new CannotWriteToFileError($path->toLocal());
        }

        //example png data url for base64:
        //'data:image/png;base64,AAAFBfj42Pj4'
    }

}