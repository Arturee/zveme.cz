<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: Artur
 * Date: 18-Jun-17
 * Time: 4:39 PM
 */

namespace App\Model;
use Nette;
use Nette\Utils\DateTime;

class Upload {
    use Nette\SmartObject;

    /**
     * more or less any field can be null
     */

    /** @var DateTime */
    private $endDate;
    /** @var  string */
    private $categoryId, $categoryNew, $thumbDataUrl, $location, $url, $name, $description, $id;
    /** @var  int */
    private $sizeGoal, $ownerId;

    /** @var  int */
    private $size = 0;
    /** @var string */
    private $status = 'initiated';

    /**
     * Upload constructor.
     * @param DateTime $endDate
     * @param $categoryId
     * @param $categoryNew
     * @param $thumbDataUrl
     * @param $location
     * @param $url
     * @param $name
     * @param $description
     * @param $sizeGoal
     * @param $ownerId
     */
    public function __construct(DateTime $endDate, $categoryId, $categoryNew, $thumbDataUrl, $location, $url,
                                $name, $description, $sizeGoal, $ownerId)
    {
        $this->endDate = $endDate;
        $this->categoryId = $categoryId;
        $this->categoryNew = $categoryNew;
        $this->thumbDataUrl = $thumbDataUrl;
        $this->location = $location;
        $this->url = $url;
        $this->name = $name;
        $this->description = $description;
        $this->sizeGoal = $sizeGoal;
        $this->ownerId = $ownerId;

        $this->id = session_id(); //microtime() . '-' .
    }

    /**
     * @param string $status
     */
    public function setStatus($status) {
        $this->status = $status;
    }
    /**
     * @param int $addition
     */
    public function increaseSize($addition){
        $this->size += $addition;
    }
    /**
     * @return bool
     */
    public function isComplete() {
        return $this->size === $this->sizeGoal;
    }




    /**
     * @return DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @return string
     */
    public function getCategoryId()
    {
        return $this->categoryId;
    }

    /**
     * @return string
     */
    public function getCategoryNew()
    {
        return $this->categoryNew;
    }

    /**
     * @return string
     */
    public function getThumbDataUrl()
    {
        return $this->thumbDataUrl;
    }

    /**
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    public function getSize() : int
    {
        return $this->size;
    }

    public function getStatus() : string
    {
        return $this->status;
    }

    public function getOwnerId() : ?int
    {
        return $this->ownerId;
    }
}