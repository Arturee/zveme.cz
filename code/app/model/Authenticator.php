<?php
namespace App\Model;

/**
 * Created by PhpStorm.
 * User: Artur
 * Date: 13-Jun-17
 * Time: 9:03 PM
 */
use Nette\Security;


class Authenticator implements Security\IAuthenticator
{
    const ROLE_ADMIN = 'admin';

    /** @var UserRepository */
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }
    /** @Override */
    public function authenticate(array $credentials)
    {
        list($username, $password) = $credentials;
        $wrap = $this->userRepository->getWrap($username);
        if (!$wrap){
            throw new Security\AuthenticationException('User not found.');
        }

        $user = $wrap->getUser();
        $passwordHashFromDb = $wrap->getPassword();
        if (!Security\Passwords::verify($password, $passwordHashFromDb)){
           throw new Security\AuthenticationException('Invalid password.');
        }

        $result = new Security\Identity($user->getId(), $user->getRole(), ['user' => $user]);
        return $result;
    }
}