# ADMIN DOCUMENTATION #
create admin user:
    - register as guest, in phpmyadmin change role to admin
        - (PERHAPS put this into DB schema script)
temp/log:
    - always delete email-sent after getting an error email
    - when some log file becomes too large:
        - create folder log-[date]
        - copy .htaccess to it
        - cut and paste log files to it
        - (logs are read-only)



# TECHNICAL DOCUMENTATION #
- this code is for Apache
    - to get it to work on Microsoft, use web.config instead of .htaccess (see nette\sandbox for web.config)
- deactivate xdebug to regain/measure speed
    - xdebug slows server speed 20x
    - (comment out zend_extension in php.ini)
- image sizes
    - thumbnail 210x297 (A4 ratio)
    - hd 1260x1782 (A4 ratio) ~ 5MB


# DESIGN DOCUMENTATION #
- roles:
    - admin
    - null (guest)
- debug
    - from home ip
    - email is sent to me otherwise
- Posters
    - posters are resized and thumbs are generated in JS
    - only A4 portrait is supported now
    - allows extension to client side cropping
- upload
    - poster data (as data url) are appended to session and saved on completion
    - metadata are in session too
- PHP Logger + exceptions
- Js.php
    - merges scripts to decrease download time
    - passes vars from PHP to JS (global object ZVEME_CZ)
    - extensible for minification
- js error handling (see init.js)
- i18n on frontend and backend
- JS split - Uic
    - can access global object ZVEME_CZ
    - can be composed of more Uics
    - can access DOM
    - uses pure classes
        - pure classes cannot access DOM or ZVEME_CZ or any Uic
- JS roles
    - according to backend role, different JS is downloaded
Roles
    - admin
        - can delete posters
    - logged-in
        - uploaded Posters are linked to him
    - null/guest
- Util\Path
    - wwwPath/systemPath
- logging
        - bug/hack/failure/non-repudiation
- fontend+backend flash messages



# SPEED #
- no xdebug
    - Homepage: 300ms Nette, 50ms receiving.
    - Upload: 300ms all. (a few forms, no access to db)
    - Detail: 250ms all. (extremely simple, 1x access to db)
    - tests.html: 15ms
    - checker.php: 30ms
- comparison:
    - Seznam.cz: 200ms block, 200ms wait, 10ms rec.
    - nette/Fifteen: 180ms Nette
    - nette/CD-collection: 200ms Nette
- future:
    - get a better hosting



# WEIRD STUFF #
- a local folder is used to manage session (instead of letting Apache/PHP handle it) because there was hope of making
upload faster on Wedos. BUT then Wedos decreased max POST request body length, so now it cannot be measured if there
was a speedup or not AND the upload remains painfully slow.






# DEPLOYMENT #
NEEDED ENVIRONMENT:
- Apache ?
- PHP 7.1
- mysql ?
- Firefox ?
- Sendmail or other?

UPDATE - STEPS:
- make sure debug mode is set to correct IP
- run deploy.sh
- copy /deployment/ to server (without deleting it's content first)
- delete /temp/cache

FIRST DEPLOYMENT - STEPS:
- delete the content of robots.txt (only when live)
- delete content of server
- [UPDATE STEPS]
- export and import mysql
- view checker
- delete checker (for security)
- make sure the following are writable PERHAPS add to checker as well
    temp
    log
    www/wall/thumb
    www/wall/hd
    www/temp
- make sure /app, /temp, /log are NOT accessbile (that mod_rewrite works)







# SECURITY #
JS/SQL injection - PREVENTED by Latte, DB wrapper
XSS - PREVENTED by Latte (impossible to create a script tag by accident)
LFI/RFI - PREVENTED by never using dynamic include (Nette router + FC is used for routing)
session hijacking - PREVENTED in Nette
CSRF - PREVENTED for all Nette forms (see EnhancedForm) via a token
URL attack, control codes, invalid UTF-8 - PREVENTED by Nette
access control bypass - PREVENTED by checking admin privileges on startup in AdminPresenter
file scanning - PREVENTED by .htaccess files and directory listing is off (in www/.htaccess)
man in the middle listening for passwords - LATER prevent
password brute-force - LATER prevent (probably prevented by a slow server already)
invalid values attack - LATER prevent



# LIST OF COOKIES #
sidebar.isOpen - bool
sidebar.login.isOpen - bool
categories.unchecked - array
(session id)

# LIST OF SESSION VARS #
upload:
    upload - Upload
    poster - Poster
(Nette user)


# CROSS-BROWSERNESS #
firefox - good
chome - good
IE - bad. cannot pick/open image file
Edge - bad. cannot pick date