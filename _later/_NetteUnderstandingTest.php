<?php
use Tester\Assert;

$dic = require_once 'bootstrap.php';


class NetteUnderstandingTest extends Tester\TestCase {
    /* @var \Nette\DI\Container */
    private $dic;

    public function setUp() {
        global $dic;
        $this->dic = $dic;
    }


    public function testSDIC_worksAndLoadsTestNeon(){
        Assert::equal(true, $this->dic->getParameters()['test']);
    }
    public function testSession_isntStartedWithoutHttpRequest(){
        $session = $this->dic->getService('session');
        Assert::false($session->isStarted());
    }
    public function testSession_canBeStartedManually(){
        $session = $this->dic->getService('session');
        $session->start();
        Assert::true($session->isStarted());
    }
    public function testSession_withoutHttpRequestEachSessionIsNewOne(){
        $session = $this->dic->getService('session');
        $session->start();
        Assert::true($session->isStarted());
        Assert::notEqual('abc5b3413fde4e3b736de13a0f9b97e8', $session->getId());
    }
    public function testDateTime_works(){
        $x = new \Nette\Utils\DateTime();
        $y = $x->modifyClone('+1 day');
        $x->modify('+1 day');
        Assert::equal($x, $y);
    }
}

$testCase = new NetteUnderstandingTest();
$testCase->run();