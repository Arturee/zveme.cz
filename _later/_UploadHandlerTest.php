<?php
use Tester\Assert;

$dic = require_once 'bootstrap.php';

class UploadHandlerTest extends Tester\TestCase {
    /* @var \Nette\DI\Container */
    private $dic;

    public function setUp() {
        /* @var \Nette\DI\Container */
        global $dic;
        $this->dic = $dic;
        //$fakeSession = $dic->getService('session');
        //$fakeSession->setFakeID()?
    }
    public function testStartReceiving_works(){
        //MEBBE this manual sessions may be a problem in future tests when it expires ...
        $this->dic->getService('session')->start();
        $uploader = $this->dic->getByType('App\Model\PosterUploader');
        //$uploader = new PosterUploader(null, null);
        $upload = $uploader->startReceiving(666,'666');
        Assert::equal('initiated', $upload->getStatus());
        Assert::equal(0, $upload->getSize());
        Assert::equal('666', $upload->getThumbDataUrl());
        //Assert::equal('', $upload->getCommunicationId());
    }
    public function testReceiveNextChunk_works(){
        $this->dic->getService('session')->start();
        //MEBBE starts new or not?
        $uploader = $this->dic->getByType('App\Model\PosterUploader');
        //$uploader = new PosterUploader(null, null);
        $upload = $uploader->startReceiving(666,'data:image/png;base64,xxx');
        $upload = $uploader->receiveNextChunk($upload->getCommunicationId(), 26, 'data:image/png;base64,abcd');
        Assert::equal('progress', $upload->getStatus());
        Assert::equal(26, $upload->getSize());
        //MEBBE check that the file exists?
    }
    public function testWholeUpload_works(){
        $this->dic->getService('session')->start();
        //MEBBE starts new or not?
        /** @var $uploader App\Model\PosterUploader */
        $uploader = $this->dic->getByType('App\Model\PosterUploader');
        $upload = $uploader->startReceiving(30,'data:image/png;base64,xxx');
        $upload = $uploader->receiveNextChunk($upload->getCommunicationId(), 26, 'data:image/png;base64,abcd');
        Assert::equal('progress', $upload->getStatus());
        $upload = $uploader->receiveNextChunk($upload->getCommunicationId(), 4, 'EFGH');
        Assert::equal('complete', $upload->getStatus());
        //TODO check the poster
    }

}

$testCase = new UploadHandlerTest();
$testCase->run();
