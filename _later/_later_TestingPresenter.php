<?php
require_once 'bootstrap.php';

use Tester\Assert;


//https://forum.nette.org/cs/14658-mockovani-nette-http-session
//https://phpfashion.com/velestrucne-testovani-presenteru-v-nette







// z DI kontejneru, který vytvořil bootstrap.php, získáme instanci PresenterFactory
$presenterFactory = $container->getByType('Nette\Application\IPresenterFactory');
// a vyrobíme presenter Sign
$presenter = $presenterFactory->createPresenter('Sign');
//A bude vhodné vypnout autoCanonicalize, aby presenter nepřesměrovával na kanonické URL:
$presenter->autoCanonicalize = FALSE;
// zobrazení stránky Sign:in metodou GET
$request = new Nette\Application\Request('Sign', 'GET', array('action' => 'in'));
$response = $presenter->run($request);

//Ověříme, zda odpověď je skutečně šablona:
Assert::type('Nette\Application\Responses\TextResponse', $response);
Assert::type('Nette\Bridges\ApplicationLatte\Template', $response->getSource());

//Necháme šablonu vygenerovat HTML kód:
$html = (string) $response->getSource();

//A nyní třeba zkontrolujeme, zda se na stránce nacházejí formulářová políčka pro jméno a heslo.
//Syntax je stejná jako u CSS selektorů.

$dom = Tester\DomQuery::fromHtml($html);
Assert::true( $dom->has('input[name="username"]') );
Assert::true( $dom->has('input[name="password"]') );


Assert::same( 'Hello John', 'Hell John' );


$this->getService('connection');


/**
 * Exception
 *
 *
 *
 *  Assert::exception(function() use ($o) {
 *      $o->say('');
 *  }, 'InvalidArgumentException', 'Invalid name');
 *
 * 
 */

