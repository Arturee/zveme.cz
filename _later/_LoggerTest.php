<?php
use Tester\Assert;
use App\Util\Logger;
use Nette\Utils\FileSystem;
use Nette\Utils\Strings;

$dic = require_once 'bootstrap.php';

/**
 * Class LoggerTest
 */
class LoggerTest extends Tester\TestCase {

    /** @var  Logger */
    private $logger;

    const LOG_FILE = 'sandbox/log/critical.log';


    /** @Override */
    public function setUp()
    {
        /** @var Nette\DI\Container $dic */
        global $dic;
        $this->logger = $dic->getByType('App\Util\Logger');
    }

//    public function testLogBug_works(){
//        $message = 'hello bug.';
//        $this->logger->logBug($message);
//        $expected = '[BUG] ' . $message;
//        $fileContent = FileSystem::read(LoggerTest::LOG_FILE);
//        Assert::true(Strings::contains($fileContent, $expected)); //since log files cannot be deleted ...
//    }
//    public function testLogHack_works(){
//        $message = 'hello hack.';
//        $this->logger->logBug($message);
//        $expected = '[HACK] ' . $message;
//        $fileContent = FileSystem::read(LoggerTest::LOG_FILE);
//        Assert::true(Strings::contains($fileContent, $expected)); //since log files cannot be deleted ...
//    }
}

$testCase = new LoggerTest();
$testCase->run();
