<?php
use App\Model\PathFactory;
use App\Model\PosterFactory;
use Tester\Assert;
use Nette\Utils\DateTime;
use App\Model\Poster;
$dic = require_once 'bootstrap.php';




class PosterFactoryTest extends Tester\TestCase {
    /** @var PathFactory */
    private $pathFactory;
    private $date;
    private $category = 'cinema';

    /** @Override */
    public function setUp(){
        $this->pathFactory = new PathFactory('C:\Users\Me');
        $this->date = new DateTime();
    }

    public function testConstructor_works(){
        new PosterFactory($this->pathFactory);
    }
    public function testCreate_works(){
        $posterFactory = new PosterFactory($this->pathFactory);
        $actual = $posterFactory->create(1, 1, 100, $this->date, $this->category);
        $expected = new Poster(1, 1, 100,
            $this->pathFactory->create('wall/hd/1.png'),
            $this->pathFactory->create('wall/thumb/1.png'),
            $this->date, $this->category);
        Assert::equal($expected, $actual);
    }
}


$testCase = new PosterFactoryTest();
$testCase->run();
