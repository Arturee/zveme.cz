

# WEDOS (NoLimit) CAPABILITIES #
- https://hosting.wedos.com/cs/webhosting/extra.html
- https://hosting.wedos.com/cs/webhosting.html LATER read
- 360 kc/year

- email max 5 GB
- db max 1 GB
- HDD unlimited
- RAM max 128 MB
- POST request max 32 MB
- file upload max 32 MB
- PHP processes max 5x
- backup (web, db, email): frequency unknown. paid recovery

- email OK
    - PHP mail() or SMTP
    - max 500 emails/day. otherwise mail() returns false
    (https://hosting.wedos.com/cs/odesilani-emailu.html)
- db OK
- composer, bower NO
- mod_rewrite OK
- HDD autorska prava




# WEDOS LIMITATIONS #
- limits: https://hosting.wedos.com/cs/webhosting/php.html
- max_input_vars = 10K bohuzel! - proto file upload je na tom velmi spatne.
    - LATER but how come localhost has 1000 and allows me to post 250 000 chars?
- phpinfo for 7.1(all wedos VMs have the same)
    - http://46.28.106.32/info.php71
    - LATER PHP seems weirdly set up - https://client.wedos.com/webhosting/configuration.html?id=146402
- folder structure: https://kb.wedos.com/cs/webhosting/adresarova-struktura.html
    - session
    - tmp
    - www
    - www/domains
    - www/subdom



# WEDOS BUGS #
- LATER server is extremely slow - (download of a simple .png is very slow. Poster uplaod is also slow)
- there is no mod_rewrite in php_info, but wedos asserts that it is installed and works correctly
- LATER shared (classic) session is possibly very slow
    (because chunked upload was 20x slower than on localhost (measured in server time, back when it was possible
    to use chunks of length 250 000 chars))
- LATER upload chunks
    - POST size is limited with no explanation




# ADMINISTRATION #

## MYSQL ##
- server name: wm129.wedos.net
- db name: d146402_zvemecz
- admin: (use with phpmyadmin)
    - a146402_zvemecz : VTsGJdMC
    - phpmyadmin: https://pma.wedos.net/
- php user: (cannot create tables)
    - w146402_zvemecz : e8QCkS44



## EMAIL ##
 - info@zveme.cz : 8fjeX#*%RPOkfJ!@$#BZ^@%!soQJIO